# WAUTA

App para el módulo Proyecto del CFGS Multiplataforma.
*****************************************************

## REQUISTOS

Dispositivo móvil con versión del kernel 7.0.0 o superior para API 29.
*****************************************************

## INSTALACIÓN

Se instalará en dispositivos móviles o tablets.
*****************************************************

## CONSTRUIDO CON

Android Studio Artic Fox 2020.3.1 Patch 3.
En lenguaje de programación Java.

![Android Studio Artic Fox 2020.3.1](androidstudioversion.PNG)

*****************************************************

## VERSIÓN

v1.0.0.
*****************************************************

## AUTOR

José Antonio Delgado.
*****************************************************

## LICENCIA

BSD.
*****************************************************

## WEB

[AGF](https://agf-ingenieros.000webhostapp.com/)

## ACTIVITY

![Inicial](activity_inicial.PNG)
******************************************************

![Logo CIP](inicial_dos.PNG)
