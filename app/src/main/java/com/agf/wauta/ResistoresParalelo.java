package com.agf.wauta;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Paint;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetBehavior;

public class ResistoresParalelo extends AppCompatActivity {

    BottomSheetBehavior behavior;
    View vistaEmergente;
    private EditText edt_R1,edt_R2,edt_R3;
    private TextView tv_salida, tv_titulo;
    private String sR1,sR2,sR3;
    private Float fR1,fR2,fR3,fRequiv;

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resistores_paralelo);

        //barra con flecha y quitar texto
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);

        //para el layout emergente
        vistaEmergente = findViewById(R.id.bottomSheet);        //Vista emergente
        behavior = BottomSheetBehavior.from(vistaEmergente);
        behavior.setState(BottomSheetBehavior.STATE_HIDDEN);    //inicial está oculta

        //titulo subrayado
        tv_titulo = findViewById(R.id.tv_1_resistorParalelo);
        tv_titulo.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);

        edt_R1 = findViewById(R.id.edt_1_resistorParalelo);
        edt_R2 = findViewById(R.id.edt_2_resistorParalelo);
        edt_R3 = findViewById(R.id.edt_3_resistorParalelo);
        tv_salida = findViewById(R.id.tv_5_resistorParalelo);
    }
    //---------------------------------------------------------------------------------------------- Para los tres botones barra navegación superior
    /**
     * Para CREAR los tres botones barra navegación superior
     * @param manu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu manu){
        getMenuInflater().inflate(R.menu.menuopcionescalculos, manu);
        return true;
    }
    //---------------------------------------------------------------------------------------------- Para los tres botones barra navegación superior
    /**
     * Para SELECCIONAR los tres botones barra navegación superior
     * @param menuItem
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem){

        if (menuItem.getItemId() != R.id.item1 && menuItem.getItemId() != R.id.item2 && menuItem.getItemId() != R.id.item3)
            onBackPressed();    //este es para la flecha izquierda del toolbar

        switch (menuItem.getItemId()){
            case R.id.item1:    //para calcular
                MediaPlayer mp1 = MediaPlayer.create(this,R.raw.buttonsounduno);
                mp1.start();
                calcularResistorParalelo();
                return true;
            case R.id.item2:    //para layout emergente
                MediaPlayer mp2 = MediaPlayer.create(this,R.raw.claroringtonestono);
                mp2.start();
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                return true;
            case R.id.item3:    //para limpiar
                Toast.makeText(getApplicationContext(),"Limpiando selección",Toast.LENGTH_SHORT).show();
                MediaPlayer mp3 = MediaPlayer.create(this,R.raw.sdalert20);
                mp3.start();
                limpiar();
                return true;
        }
        return true;
    }
    //---------------------------------------------------------------------------------------------- botón dentro de layout emergente~
    /**
     * botón dentro de layout emergente~
     * @param view
     */
    public void cerrarSheet(View view){
        behavior.setState(BottomSheetBehavior.STATE_HIDDEN);
    }
    //----------------------------------------------------------------------------------------------
    /**
     * Método para limpiar contenido de
     */
    private void limpiar() {
        edt_R1.setText("");
        edt_R2.setText("");
        edt_R3.setText("");
        tv_salida.setText("_");
    }
    //---------------------------------------------------------------------------------------------- método para evitar excepccion edit-text vacío
    /**
     * método para evitar excepccion edit-text vacío
     * @param _num
     * @return
     */
    public float checkNumero(String _num){
        float resultado = 0.0f;
        try {
            if (_num!=null){
                resultado = Float.parseFloat(_num);
            }
        }catch (NumberFormatException e){
            tv_salida.setText("_");
            Toast mytoasErrot = Toast.makeText(getApplicationContext(),"<Error campo de entrada de datos vacío>",Toast.LENGTH_SHORT);
            mytoasErrot.setGravity(Gravity.CENTER_VERTICAL,0,0);
            mytoasErrot.show();
            MediaPlayer mp4 = MediaPlayer.create(this,R.raw.miedodos);
            mp4.start();
        }
        return resultado;
    }
    //----------------------------------------------------------------------------------------------
    /**
     * método para calcular resistencias en paralelo
     */
    private void calcularResistorParalelo() {
        sR1 = edt_R1.getText().toString();
        fR1 = checkNumero(sR1);
        sR2 = edt_R2.getText().toString();
        fR2 = checkNumero(sR2);
        sR3 = edt_R3.getText().toString();
        fR3 = checkNumero(sR3);

        fRequiv = 1 / (1/fR1 + 1/fR2 + 1/fR3);

        //para determinar si campos están vacío no represetar valor NULL en layout
        if (fRequiv==null){
            tv_salida.setText("_");
        }else{
            activaAnimacion(fRequiv);
        }
    }
    //---------------------------------------------------------------------------------------------- método para activar animación de salida en TextView
    /**
     * método para activar animación de salida en TextView
     * @param _dato
     */
    public void activaAnimacion(Float _dato){
        tv_salida.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade_suave));
        tv_salida.setText(String.valueOf("R.equiv = "+_dato) + " Ω");
    }
}