package com.agf.wauta;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Paint;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetBehavior;

public class PotenciaAparente extends AppCompatActivity {

    BottomSheetBehavior behavior;
    View  vistaEmergente;
    private RadioGroup rg_potAparente;
    private RadioButton rb_1,rb_2,rb_3;
    private EditText edt_voltaje, edt_corriente;
    private TextView tv_resultado, tv_titulo;
    private String sConrriente,sVoltio;
    private Float fCorriente,fVoltio,fPotencia;

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_potencia_aparente);

        //barra con flecha y quitar texto
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);

        //para el layout emergente
        vistaEmergente = findViewById(R.id.bottomSheet);        //Vista emergente
        behavior = BottomSheetBehavior.from(vistaEmergente);
        behavior.setState(BottomSheetBehavior.STATE_HIDDEN);    //inicial está oculta

        //titulo subrayado
        tv_titulo = findViewById(R.id.tv_potenciaAparente_tit);
        tv_titulo.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);

        rg_potAparente = findViewById(R.id.rg_potenciaAparente);
        rb_1 = findViewById(R.id.rb_1_potAparente);
        rb_2 = findViewById(R.id.rb_2_potAparente);
        rb_3 = findViewById(R.id.rb_3_potAparente);
        edt_voltaje = findViewById(R.id.edt_01_potAparente);
        edt_corriente = findViewById(R.id.edt_02_potAparente);
        tv_resultado = findViewById(R.id.tv_03_potAparente);
    }
    //----------------------------------------------------------------------------------------------
    /**
     * menú contextual
     * @param manu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu manu){
        getMenuInflater().inflate(R.menu.menuopcionescalculos, manu);
        return true;
    }
    //---------------------------------------------------------------------------------------------- Para los tres botones barra navegación superior
    /**
     * Para los tres botones barra navegación superior
     * @param menuItem
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem){

        if (menuItem.getItemId() != R.id.item1 && menuItem.getItemId() != R.id.item2 && menuItem.getItemId() != R.id.item3)
            onBackPressed();    //este es para la flecha izquierda del toolbar

        switch (menuItem.getItemId()){
            case R.id.item1:    //para calcular
                MediaPlayer mp1 = MediaPlayer.create(this,R.raw.buttonsounduno);
                mp1.start();
                calculaPotenciaAparente();
                return true;
            case R.id.item2:    //para layout emergente
                MediaPlayer mp2 = MediaPlayer.create(this,R.raw.claroringtonestono);
                mp2.start();
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                return true;
            case R.id.item3:    //para limpiar
                Toast.makeText(this,"Limpiando selección",Toast.LENGTH_SHORT).show();
                MediaPlayer mp3 = MediaPlayer.create(this,R.raw.sdalert20);
                mp3.start();
                limpiar();
                return true;
        }
        return true;
    }
    //---------------------------------------------------------------------------------------------- método para limpiar todos los elementos del layout
    /**
     * método para limpiar todos los elementos del layout
     */
    private void limpiar() {
        rg_potAparente.clearCheck();
        edt_corriente.setText("");
        edt_voltaje.setText("");
    }
    //---------------------------------------------------------------------------------------------- método para evitar excepccion edit-text vacío
    /**
     * método para evitar excepccion edit-text vacío
     * @param _num
     * @return
     */
    public float checkNumero(String _num){
        float resultado = 0.0f;
        try {
            if (_num!=null){
                resultado = Float.parseFloat(_num);
            }
        }catch (NumberFormatException e){
            Toast mytoasErrot = Toast.makeText(this,"Error campo vacío",Toast.LENGTH_LONG);
            mytoasErrot.setGravity(Gravity.CENTER,0,0);
            mytoasErrot.show();
            MediaPlayer mp4 = MediaPlayer.create(this,R.raw.miedodos);
            mp4.start();
        }
        return resultado;
    }
    //---------------------------------------------------------------------------------------------- método para calcular potencia aparente
    /**
     * método para calcular potencia aparente
     */
    private void calculaPotenciaAparente() {
        if (rb_1.isChecked()){
            //opción a 230 V
            sConrriente = edt_corriente.getText().toString();
            fCorriente = checkNumero(sConrriente);
            fPotencia = fCorriente * 230;
            activaAnimacion(fPotencia);
        }else if (rb_2.isChecked()){
            //opción a 400 V
            sConrriente = edt_corriente.getText().toString();
            fCorriente = checkNumero(sConrriente);
            fPotencia = fCorriente * 400;
            activaAnimacion(fPotencia);
        }else if (rb_3.isChecked()){
            //opción libre
            sConrriente = edt_corriente.getText().toString();
            fCorriente = checkNumero(sConrriente);
            sVoltio = edt_voltaje.getText().toString();
            fVoltio = checkNumero(sVoltio);
            fPotencia = fVoltio * fCorriente;
            activaAnimacion(fPotencia);
        }else {
            Toast myToast = Toast.makeText(PotenciaAparente.this,"Debes seleccionar una opción",Toast.LENGTH_LONG);
            myToast.setGravity(Gravity.CENTER,0,0);
            myToast.show();
        }
    }
    //---------------------------------------------------------------------------------------------- método para activar animación de salida en TextView
    /**
     * método para activar animación de salida en TextView
     * @param _potencia
     */
    public void activaAnimacion(Float _potencia){
        tv_resultado.setText(String.valueOf(_potencia) + " VA");
        tv_resultado.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade_suave));
    }
    //---------------------------------------------------------------------------------------------- botón dentro de layout emergente~
    /**
     * botón dentro de layout emergente~
     * @param view
     */
    public void cerrarSheet(View view){
        behavior.setState(BottomSheetBehavior.STATE_HIDDEN);
    }
}