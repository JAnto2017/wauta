package com.agf.wauta;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.widget.TextView;

public class SensorLuz extends AppCompatActivity implements SensorEventListener {

    private SensorManager mSensorManager;
    private Sensor mSensor;
    private TextView valoresSenLuz;
    private MediaPlayer mp;

    //----------------------------------------------------------------------------------------------

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor);

        //quitar la barra superior de la vista
        getSupportActionBar().hide();

        //activamos el sensor de luz
        mSensorManager =(SensorManager) getSystemService(Context.SENSOR_SERVICE);
        if (mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT) != null) {
            mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        }

        //TextView para representar datos
        valoresSenLuz = findViewById(R.id.tv_01_sensor);

        //activamos sonido kit coche fantastico
        mp = MediaPlayer.create(this,R.raw.kitfantastic);
        mp.start();

        //animación loading

    }
    //----------------------------------------------------------------------------------------------

    /**
     * determinar cambios en los parámetros del sensor
     * @param sensorEvent
     */
    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        valoresSenLuz.setText(String.valueOf(sensorEvent.values[0]));
    }
    //----------------------------------------------------------------------------------------------

    /**
     *
     * @param sensor
     * @param i
     */
    @Override
    public void onAccuracyChanged(android.hardware.Sensor sensor, int i) {

    }
    //----------------------------------------------------------------------------------------------

    /**
     * para el sensor cuando activity pasa a segundo plano
     */
    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_NORMAL);
    }
    //----------------------------------------------------------------------------------------------

    /**
     * para pausar el uso del sensor cuando está en segudno plano
     */
    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
        mp.pause();
    }
    //----------------------------------------------------------------------------------------------

    /**
     * para parar el uso del sensor cuando está parada la actividad
     */
    @Override
    protected void onStop() {
        super.onStop();
        mp.stop();
    }
}