package com.agf.wauta;

import androidx.appcompat.app.AppCompatActivity;

import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class Area extends AppCompatActivity {

    private EditText edt_area;
    private String sDato;
    private float fDato;
    private Spinner spinner;
    private TextView tv1,tv2,tv3,tv4,tv5,tv6,tv7,tv8,tv9,tv10,tv11,tv12,tv13,tv14,tv15;
    FloatingActionButton btnFloatCalcular;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_area);

        //fija vista en portrait evitar landscape
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //quitar la barra superior de la vista
        getSupportActionBar().hide();

        edt_area = findViewById(R.id.edt_area);
        tv1 = findViewById(R.id.tv_2_area);
        tv2 = findViewById(R.id.tv_4_area);
        tv3 = findViewById(R.id.tv_6_area);
        tv4 = findViewById(R.id.tv_8_area);
        tv5 = findViewById(R.id.tv_10_area);
        tv6 = findViewById(R.id.tv_12_area);
        tv7 = findViewById(R.id.tv_14_area);
        tv8 = findViewById(R.id.tv_16_area);
        tv9 = findViewById(R.id.tv_18_area);
        tv10= findViewById(R.id.tv_20_area);
        tv11= findViewById(R.id.tv_22_area);
        tv12= findViewById(R.id.tv_24_area);
        tv13= findViewById(R.id.tv_26_area);
        tv14= findViewById(R.id.tv_28_area);
        tv15= findViewById(R.id.tv_30_area);
        btnFloatCalcular = findViewById(R.id.idFabCal);

        //para el spinner
        spinner = findViewById(R.id.spinner);
        String[] opciones = {"Kilómetro cuadrado","Hectómetro cuadrado","Decámetro cuadrado","Metro cuadrado",
        "Decímetro cuadrado","Centímetro cuadrado","Milímetro cuadrado","Hectárea","Áreas","Acre","Pie cuadrado",
        "Yarda cuadrada","Pulgada cuadrada","Pulgada circular","Barn"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, opciones);
        spinner.setAdapter(adapter);

        //evento para el botón flotante
        btnFloatCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                seleccSpinner();
            }
        });
    }
    //---------------------------------------------------------------------------------------------- método para evitar excepccion edit vacío
    /**
     * método para evitar excepccion edit vacío
     * @param _num
     * @return
     */
    public float checkNumero(String _num){
        float resultado = 0f;
        try {
            if (_num!=null){
                resultado = Float.parseFloat(_num);
            }
        }catch (NumberFormatException e){
            Toast.makeText(this,"Error campo vacío",Toast.LENGTH_SHORT).show();
        }
        return resultado;
    }
    //---------------------------------------------------------------------------------------------- método para determinar selec dek spinner
    /**
     * método para determinar selec dek spinner
     */
    public void seleccSpinner() {
        //activa sonido con click botón flotante
        MediaPlayer mp = MediaPlayer.create(this, R.raw.buttonsounduno);
        mp.start();

        //convertimos dato de entrada de string -> float
        sDato = edt_area.getText().toString();
        fDato = checkNumero(sDato);

        String seleccion = spinner.getSelectedItem().toString();

        if (seleccion.equals("Kilómetro cuadrado")){
            if (fDato!=0f){
                calculaArea(1,fDato);
            }else{
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }else if (seleccion.equals("Hectómetro cuadrado")){
            if (fDato==0.0){
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }else{
                calculaArea(2,fDato);
            }
        }else if (seleccion.equals("Decámetro cuadrado")){
            if (fDato==0.0){
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }else{
                calculaArea(3,fDato);
            }
        }else if (seleccion.equals("Metro cuadrado")){
            if (fDato==0.0){
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }else{
                calculaArea(4,fDato);
            }
        }else if (seleccion.equals("Decímetro cuadrado")){
            if (fDato==0.0){
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }else{
                calculaArea(5,fDato);
            }
        }else if (seleccion.equals("Centímetro cuadrado")){
            if (fDato==0.0){
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }else{
                calculaArea(6,fDato);
            }
        }else if (seleccion.equals("Milímetro cuadrado")){
            if (fDato==0.0){
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }else{
                calculaArea(7,fDato);
            }
        }else if (seleccion.equals("Hectárea")){
            if (fDato==0.0){
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }else{
                calculaArea(8,fDato);
            }
        }else if (seleccion.equals("Áreas")){
            if (fDato==0.0){
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }else{
                calculaArea(9,fDato);
            }
        }else if (seleccion.equals("Acre")){
            if (fDato==0.0){
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }else{
                calculaArea(10,fDato);
            }
        }else if (seleccion.equals("Pie cuadrado")){
            if (fDato==0.0){
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }else{
                calculaArea(11,fDato);
            }
        }else if (seleccion.equals("Yarda cuadrada")){
            if (fDato==0.0){
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }else{
                calculaArea(12,fDato);
            }
        }else if (seleccion.equals("Pulgada cuadrada")){
            if (fDato==0.0){
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }else{
                calculaArea(13,fDato);
            }
        }else if (seleccion.equals("Pulgada circular")){
            if (fDato==0.0){
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }else{
                calculaArea(14,fDato);
            }
        }else if (seleccion.equals("Barn")){
            if (fDato==0.0){
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }else{
                calculaArea(15,fDato);
            }
        }
    }

    //----------------------------------------------------------------------------------------------
    /**
     * km2, hm2, dam2, m2, dm2, cm2, mm2, ha, a, ac, ft2, yd2, in2, pul-circ, barn
     * @param i
     * @param fDato
     */
    private void calculaArea(int i, float fDato) {
        switch (i){
            case 1: //kilometro cuadrado
                tv1.setText(String.valueOf(fDato));
                tv2.setText(String.valueOf(fDato*100));
                tv3.setText(String.valueOf(fDato*10000));
                tv4.setText(String.valueOf(fDato*1000000));
                tv5.setText(String.valueOf(fDato*100000000));
                tv6.setText(String.valueOf(fDato*1e+10));
                tv7.setText(String.valueOf(fDato*1e+12));
                tv8.setText(String.valueOf(fDato*100));
                tv9.setText(String.valueOf(fDato*10000));
                tv10.setText(String.valueOf(fDato*247.10538));
                tv11.setText(String.valueOf(fDato*10763910.41671));
                tv12.setText(String.valueOf(fDato*1195990.0463));
                tv13.setText(String.valueOf(fDato*1550003100.0062));
                tv14.setText(String.valueOf(fDato*1973525241));
                tv15.setText(String.valueOf(fDato*1e+34));
                break;
            case 2: //hectometro cuadrado
                tv1.setText(String.valueOf(fDato*0.01));
                tv2.setText(String.valueOf(fDato));
                tv3.setText(String.valueOf(fDato*100));
                tv4.setText(String.valueOf(fDato*10000));
                tv5.setText(String.valueOf(fDato*1e+6));
                tv6.setText(String.valueOf(fDato*1e+8));
                tv7.setText(String.valueOf(fDato*1e+10));
                tv8.setText(String.valueOf(fDato));
                tv9.setText(String.valueOf(fDato*100));
                tv10.setText(String.valueOf(fDato*2.4710538146));
                tv11.setText(String.valueOf(fDato*107639.104167));
                tv12.setText(String.valueOf(fDato*11959.900463011));
                tv13.setText(String.valueOf(fDato*1550031.000062));
                tv14.setText(String.valueOf(fDato*19735252));
                tv15.setText(String.valueOf(fDato*1e+32));
                break;
            case 3: //decametro cuadrado
                tv1.setText(String.valueOf(fDato*0.0001));
                tv2.setText(String.valueOf(fDato*0.01));
                tv3.setText(String.valueOf(fDato));
                tv4.setText(String.valueOf(fDato*100));
                tv5.setText(String.valueOf(fDato*10000));
                tv6.setText(String.valueOf(fDato*1e+6));
                tv7.setText(String.valueOf(fDato*1e+8));
                tv8.setText(String.valueOf(fDato*0.01));
                tv9.setText(String.valueOf(fDato));
                tv10.setText(String.valueOf(fDato*0.0247105381));
                tv11.setText(String.valueOf(fDato*1076.391041671));
                tv12.setText(String.valueOf(fDato*119.59900463011));
                tv13.setText(String.valueOf(fDato*155000.31000062));
                tv14.setText(String.valueOf(fDato*197352.524139));
                tv15.setText(String.valueOf(fDato*1e+30));
                break;
            case 4: //metro cuadrado
                tv1.setText(String.valueOf(fDato*1.0e-6));
                tv2.setText(String.valueOf(fDato*0.0001));
                tv3.setText(String.valueOf(fDato*0.01));
                tv4.setText(String.valueOf(fDato));
                tv5.setText(String.valueOf(fDato*100));
                tv6.setText(String.valueOf(fDato*10000));
                tv7.setText(String.valueOf(fDato*1.0e+6));
                tv8.setText(String.valueOf(fDato*0.0001));
                tv9.setText(String.valueOf(fDato*0.01));
                tv10.setText(String.valueOf(fDato*0.00024710538146));
                tv11.setText(String.valueOf(fDato*10.76391041671));
                tv12.setText(String.valueOf(fDato*1.1959900463011));
                tv13.setText(String.valueOf(fDato*1550.0031000062));
                tv14.setText(String.valueOf(fDato*1973.52524139));
                tv15.setText(String.valueOf(fDato*1.0e+28));
                break;
            case 5: //decimetro cuadrado
                tv1.setText(String.valueOf(fDato*1.0e-8));
                tv2.setText(String.valueOf(fDato*1.0e-6));
                tv3.setText(String.valueOf(fDato*0.0001));
                tv4.setText(String.valueOf(fDato*0.01));
                tv5.setText(String.valueOf(fDato));
                tv6.setText(String.valueOf(fDato*100));
                tv7.setText(String.valueOf(fDato*10000));
                tv8.setText(String.valueOf(fDato*1.0e-6));
                tv9.setText(String.valueOf(fDato*0.0001));
                tv10.setText(String.valueOf(fDato*2.4710538146));
                tv11.setText(String.valueOf(fDato*0.107639104167));
                tv12.setText(String.valueOf(fDato*0.0119599004630));
                tv13.setText(String.valueOf(fDato*15.500031000062));
                tv14.setText(String.valueOf(fDato*19.7352524139));
                tv15.setText(String.valueOf(fDato*1.0e+26));
                break;
            case 6: //centimetro cuadrado
                tv1.setText(String.valueOf(fDato*1.0e-10));
                tv2.setText(String.valueOf(fDato*1.0e-8));
                tv3.setText(String.valueOf(fDato*1.0e-6));
                tv4.setText(String.valueOf(fDato*0.0001));
                tv5.setText(String.valueOf(fDato*0.01));
                tv6.setText(String.valueOf(fDato));
                tv7.setText(String.valueOf(fDato*100));
                tv8.setText(String.valueOf(fDato*1.0e-8));
                tv9.setText(String.valueOf(fDato*1.0e-6));
                tv10.setText(String.valueOf(fDato*2.47105381467e-8));
                tv11.setText(String.valueOf(fDato*0.00107639104));
                tv12.setText(String.valueOf(fDato*0.000119599004));
                tv13.setText(String.valueOf(fDato*0.15500031000062));
                tv14.setText(String.valueOf(fDato*0.197352524139));
                tv15.setText(String.valueOf(fDato*1.0e+24));
                break;
            case 7: //milimetro cuadrado
                tv1.setText(String.valueOf(fDato*1.0e-12));
                tv2.setText(String.valueOf(fDato*1.0e-10));
                tv3.setText(String.valueOf(fDato*1.0e-8));
                tv4.setText(String.valueOf(fDato*1.0e-6));
                tv5.setText(String.valueOf(fDato*0.0001));
                tv6.setText(String.valueOf(fDato*0.01));
                tv7.setText(String.valueOf(fDato));
                tv8.setText(String.valueOf(fDato*1.0e-10));
                tv9.setText(String.valueOf(fDato*1.0e-8));
                tv10.setText(String.valueOf(fDato*2.4710538146e-10));
                tv11.setText(String.valueOf(fDato*1.07639104167e-5));
                tv12.setText(String.valueOf(fDato*1.19599e-6));
                tv13.setText(String.valueOf(fDato*0.0015500031));
                tv14.setText(String.valueOf(fDato*0.0019735252));
                tv15.setText(String.valueOf(fDato*1.0e+22));
                break;
            case 8: //hectarea
                tv1.setText(String.valueOf(fDato*0.01));
                tv2.setText(String.valueOf(fDato));
                tv3.setText(String.valueOf(fDato*100));
                tv4.setText(String.valueOf(fDato*10000));
                tv5.setText(String.valueOf(fDato*1.0e+6));
                tv6.setText(String.valueOf(fDato*1.0e+8));
                tv7.setText(String.valueOf(fDato*1.0e+10));
                tv8.setText(String.valueOf(fDato));
                tv9.setText(String.valueOf(fDato*100));
                tv10.setText(String.valueOf(fDato*2.4710538146717));
                tv11.setText(String.valueOf(fDato*107639.1041671));
                tv12.setText(String.valueOf(fDato*11959.900463011));
                tv13.setText(String.valueOf(fDato*15500031.000062));
                tv14.setText(String.valueOf(fDato*19735252.4139));
                tv15.setText(String.valueOf(fDato*1.0e+32));
                break;
            case 9: //areas
                tv1.setText(String.valueOf(fDato*0.0001));
                tv2.setText(String.valueOf(fDato*0.01));
                tv3.setText(String.valueOf(fDato));
                tv4.setText(String.valueOf(fDato*100));
                tv5.setText(String.valueOf(fDato*10000));
                tv6.setText(String.valueOf(fDato*1.0e+6));
                tv7.setText(String.valueOf(fDato*1.0e+8));
                tv8.setText(String.valueOf(fDato*0.01));
                tv9.setText(String.valueOf(fDato));
                tv10.setText(String.valueOf(fDato*0.024710538));
                tv11.setText(String.valueOf(fDato*1076.39104167));
                tv12.setText(String.valueOf(fDato*119.59900463));
                tv13.setText(String.valueOf(fDato*155000.31));
                tv14.setText(String.valueOf(fDato*197352.52));
                tv15.setText(String.valueOf(fDato*1.0e+30));
                break;
            case 10: //acre
                tv1.setText(String.valueOf(fDato*0.00404685));
                tv2.setText(String.valueOf(fDato*0.40468564));
                tv3.setText(String.valueOf(fDato*40.468564));
                tv4.setText(String.valueOf(fDato*4046.8564));
                tv5.setText(String.valueOf(fDato*404685.64));
                tv6.setText(String.valueOf(fDato*404685640.22));
                tv7.setText(String.valueOf(fDato*4046856422.4));
                tv8.setText(String.valueOf(fDato*0.40468564));
                tv9.setText(String.valueOf(fDato*40.468564));
                tv10.setText(String.valueOf(fDato));
                tv11.setText(String.valueOf(fDato*43560));
                tv12.setText(String.valueOf(fDato*4840));
                tv13.setText(String.valueOf(fDato*6272640));
                tv14.setText(String.valueOf(fDato*7986573.298));
                tv15.setText(String.valueOf(fDato*4.046e+31));
                break;
            case 11: //pie cuadrado
                tv1.setText(String.valueOf(fDato*9.29e-8));
                tv2.setText(String.valueOf(fDato*9.29e-6));
                tv3.setText(String.valueOf(fDato*0.000929));
                tv4.setText(String.valueOf(fDato*0.0929));
                tv5.setText(String.valueOf(fDato*9.29));
                tv6.setText(String.valueOf(fDato*929.03));
                tv7.setText(String.valueOf(fDato*92903.04));
                tv8.setText(String.valueOf(fDato*9.29e-6));
                tv9.setText(String.valueOf(fDato*0.000929));
                tv10.setText(String.valueOf(fDato*2.2956e-5));
                tv11.setText(String.valueOf(fDato));
                tv12.setText(String.valueOf(fDato*0.111111));
                tv13.setText(String.valueOf(fDato*144));
                tv14.setText(String.valueOf(fDato*183.3464));
                tv15.setText(String.valueOf(fDato*9.29e+26));
                break;
            case 12: //yarda cuadrada
                tv1.setText(String.valueOf(fDato*8.36e-7));
                tv2.setText(String.valueOf(fDato*8.36e-5));
                tv3.setText(String.valueOf(fDato*0.008361));
                tv4.setText(String.valueOf(fDato*0.83612));
                tv5.setText(String.valueOf(fDato*83.6127));
                tv6.setText(String.valueOf(fDato*8361.27));
                tv7.setText(String.valueOf(fDato*836127.36));
                tv8.setText(String.valueOf(fDato*8.36e-5));
                tv9.setText(String.valueOf(fDato*0.008361));
                tv10.setText(String.valueOf(fDato*0.0002066));
                tv11.setText(String.valueOf(fDato*9));
                tv12.setText(String.valueOf(fDato));
                tv13.setText(String.valueOf(fDato*1296));
                tv14.setText(String.valueOf(fDato*1650.1185));
                tv15.setText(String.valueOf(fDato*8.361e+27));
                break;
            case 13: //pulgada cuadrada
                tv1.setText(String.valueOf(fDato*6.45e-10));
                tv2.setText(String.valueOf(fDato*6.45e-8));
                tv3.setText(String.valueOf(fDato*6.45e-6));
                tv4.setText(String.valueOf(fDato*0.000645));
                tv5.setText(String.valueOf(fDato*0.064516));
                tv6.setText(String.valueOf(fDato*6.4516));
                tv7.setText(String.valueOf(fDato*645.16));
                tv8.setText(String.valueOf(fDato*6.45e-8));
                tv9.setText(String.valueOf(fDato*6.45e-6));
                tv10.setText(String.valueOf(fDato*1.594e-7));
                tv11.setText(String.valueOf(fDato*0.006944));
                tv12.setText(String.valueOf(fDato*0.0007716));
                tv13.setText(String.valueOf(fDato));
                tv14.setText(String.valueOf(fDato*1.273239));
                tv15.setText(String.valueOf(fDato*6.4516e+24));
                break;
            case 14: //pulgada circular
                tv1.setText(String.valueOf(fDato*5.067e-10));
                tv2.setText(String.valueOf(fDato*5.067e-8));
                tv3.setText(String.valueOf(fDato*0.000005067));
                tv4.setText(String.valueOf(fDato*0.0005067));
                tv5.setText(String.valueOf(fDato*0.05067));
                tv6.setText(String.valueOf(fDato*5.067));
                tv7.setText(String.valueOf(fDato*506.707));
                tv8.setText(String.valueOf(fDato*5.067e-8));
                tv9.setText(String.valueOf(fDato*0.000005067));
                tv10.setText(String.valueOf(fDato*1.2521e-7));
                tv11.setText(String.valueOf(fDato*0.00545415));
                tv12.setText(String.valueOf(fDato*0.0006060));
                tv13.setText(String.valueOf(fDato*0.785398));
                tv14.setText(String.valueOf(fDato));
                tv15.setText(String.valueOf(fDato*5.067e+24));
                break;
            case 15: //barn
                tv1.setText(String.valueOf(fDato*1e-34));
                tv2.setText(String.valueOf(fDato*1e-32));
                tv3.setText(String.valueOf(fDato*1e-30));
                tv4.setText(String.valueOf(fDato*1e-28));
                tv5.setText(String.valueOf(fDato*1e-26));
                tv6.setText(String.valueOf(fDato*1e-24));
                tv7.setText(String.valueOf(fDato*1e-22));
                tv8.setText(String.valueOf(fDato*1e-32));
                tv9.setText(String.valueOf(fDato*1e-30));
                tv10.setText(String.valueOf(fDato*2.47e-32));
                tv11.setText(String.valueOf(fDato*1.076e-27));
                tv12.setText(String.valueOf(fDato*1.1959e-28));
                tv13.setText(String.valueOf(fDato*1.55e-25));
                tv14.setText(String.valueOf(fDato*1.9735e-25));
                tv15.setText(String.valueOf(fDato));
                break;
        }
    }
}