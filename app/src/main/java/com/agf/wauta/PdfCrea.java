package com.agf.wauta;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PdfCrea extends AppCompatActivity {

    private Button btn_PDF;
    private EditText edt_email,edt_nombre,edt_apellido,edt_cfmodulo,edt_estaprendizaje;
    private EditText edt_recurso,edt_tarearealiz;
    private ImageView imageView;

    Bitmap imgBitmap, scalebmp;
    private String asunto, mensaje, titulo, emailTo;
    //---------------------------------------------------------------------------------------------- onCreate
    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf);

        //fija vista en portrait evitar landscape
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //quitar la barra superior de la vista
        getSupportActionBar().hide();

        edt_email = findViewById(R.id.edtx_01_pdf_email);
        edt_nombre = findViewById(R.id.edtx_02_nombre);
        edt_apellido = findViewById(R.id.edtx_03_apellido);
        edt_cfmodulo = findViewById(R.id.edtx_04_cfmodulo);
        edt_estaprendizaje = findViewById(R.id.edtx_05_estaprendiz);
        edt_recurso = findViewById(R.id.edts_06_recurso);
        edt_tarearealiz = findViewById(R.id.edtx_07_tarearealizada);
        btn_PDF = findViewById(R.id.btn_01_pdf);
        imageView = findViewById(R.id.imageView);

        //activar permisos de escritura
        ActivityCompat.requestPermissions(this, new String[]{
                Manifest.permission.WRITE_EXTERNAL_STORAGE}, PackageManager.PERMISSION_GRANTED);

        lanzamosCrearPDF();
    }

    //---------------------------------------------------------------------------------------------- PDF
    /**
     * método crear los documentos PDF
     */
    private void lanzamosCrearPDF() {

        btn_PDF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //sonido btn
                MediaPlayer.create(PdfCrea.this, R.raw.sdalert7).start();

                //pdf
                PdfDocument myPDFdoc = new PdfDocument();
                Paint myPaint = new Paint();
                PdfDocument.PageInfo myPageInfo = new PdfDocument.PageInfo.Builder(595,842,1).create();
                PdfDocument.Page myPage1 = myPDFdoc.startPage(myPageInfo);
                Canvas canvas = myPage1.getCanvas();

                //imagen desde archivo para encabezado

                //Fecha
                Date dateObj = new Date();
                DateFormat dateFormat = new SimpleDateFormat("dd/MM/yy");

                canvas.drawLine(10,10,585,10,myPaint);  //línea horizontal superior
                canvas.drawLine(20,5,10,835,myPaint);   //Línea vertical izquierda
                myPaint.setStrokeWidth(2.0f);

                myPaint.setTextSize(12f);
                myPaint.setColor(Color.rgb(1,1,223));   //azul
                canvas.drawText("Fecha / Email: ",25,60,myPaint);
                myPaint.setColor(Color.rgb(4,49,180));  //azul marino
                myPaint.setTextSize(11f);
                canvas.drawText(dateFormat.format(dateObj),27,70,myPaint);
                canvas.drawText(String.valueOf(edt_email.getText()),27,80,myPaint);

                myPaint.setTextSize(12f);
                myPaint.setColor(Color.rgb(1,1,223));   //azul
                canvas.drawText("Nombre: ",25,120,myPaint);
                myPaint.setColor(Color.rgb(4,49,180));  //azul marino
                myPaint.setTextSize(11f);
                canvas.drawText(String.valueOf(edt_nombre.getText()),27,130,myPaint);

                myPaint.setTextSize(12f);
                myPaint.setColor(Color.rgb(1,1,223));   //azul
                canvas.drawText("Apellido: ",25,200,myPaint);
                myPaint.setColor(Color.rgb(4,49,180));  //azul marino
                myPaint.setTextSize(11f);
                canvas.drawText(String.valueOf(edt_apellido.getText()),27,210,myPaint);

                myPaint.setTextSize(12f);
                myPaint.setColor(Color.rgb(1,1,223));   //azul
                canvas.drawText("CF / Módulo: ",25,250,myPaint);
                myPaint.setColor(Color.rgb(4,49,180));  //azul marino
                myPaint.setTextSize(11f);
                canvas.drawText(String.valueOf(edt_cfmodulo.getText()), 27,260,myPaint);

                myPaint.setTextSize(12f);
                myPaint.setColor(Color.rgb(1,1,223));   //azul
                canvas.drawText("Estándar de Aprendizaje: ",25,300,myPaint);
                myPaint.setColor(Color.rgb(4,49,180));  //azul marino
                myPaint.setTextSize(11f);
                canvas.drawText(String.valueOf(edt_estaprendizaje.getText()), 27,310,myPaint);

                myPaint.setTextSize(12f);
                myPaint.setColor(Color.rgb(1,1,223));   //azul
                canvas.drawText("Recursos: ",25,360,myPaint);
                myPaint.setColor(Color.rgb(4,49,180));  //azul marino
                myPaint.setTextSize(11f);
                canvas.drawText(String.valueOf(edt_recurso.getText()), 27,370,myPaint);

                myPaint.setTextSize(12f);
                myPaint.setColor(Color.rgb(1,1,223));   //azul
                canvas.drawText("Tareas Realizadas: ",25,410,myPaint);
                myPaint.setColor(Color.rgb(4,49,180));  //azul marino
                myPaint.setTextSize(11f);
                canvas.drawText(String.valueOf(edt_tarearealiz.getText()), 27,420,myPaint);

                canvas.drawLine(10,490,585,490,myPaint);  //línea horizontal superior
                canvas.drawRect(15,500,800,550,myPaint); //rectángulo

                //imagen desde cámara de fotografia
                scalebmp = Bitmap.createScaledBitmap(imgBitmap,100,150,false);
                canvas.drawBitmap(scalebmp,40,570,myPaint);

                myPDFdoc.finishPage(myPage1);
                File file = new File(Environment.getExternalStorageDirectory(),"/informePDF.pdf");
                try {
                    myPDFdoc.writeTo(new FileOutputStream(file));
                    Toast toast = Toast.makeText(PdfCrea.this,"Éxito al crear PDF",Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER,0,0);
                    toast.show();
                }catch (IOException e){
                    e.printStackTrace();
                    Toast toast = Toast.makeText(PdfCrea.this,"Error al crear PDF",Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER,0,0);
                    toast.show();
                }
                myPDFdoc.close();
            }
        });

    }
    //---------------------------------------------------------------------------------------------- FOTOGRAFÍA
    /**
     * Método para FOTOGRAFÍA
     * @param view
     */
    public void tomaFoto (View view) {
        Intent intentFoto = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intentFoto.resolveActivity(getPackageManager()) != null){
            startActivityForResult(intentFoto,1);
        }
        Toast toast = Toast.makeText(this,"Realizar Fotografía",Toast.LENGTH_SHORT);
        toast.show();
    }
    protected void onActivityResult(int requestCode,int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            imgBitmap = (Bitmap) extras.get("data");
            imageView.setImageBitmap(imgBitmap);
        }
    }
    //---------------------------------------------------------------------------------------------- BORRAR EDITTEXT
    /**
     * BORRAR CONTENIDO DEL EDITTEXT
     * @param view
     */
    public void borrarEditText (View view){
        edt_email.setText("");
        edt_nombre.setText("");
        edt_apellido.setText("");
        edt_tarearealiz.setText("");
        edt_recurso.setText("");
        edt_estaprendizaje.setText("");
        edt_cfmodulo.setText("");

        Toast toast = Toast.makeText(this,"Formulario limpio",Toast.LENGTH_SHORT);
        toast.show();
    }
    //---------------------------------------------------------------------------------------------- ENVIAR EMAIL
    /**
     * ENVIAR EMAIL
     * @param view
     */
    public void enviarEmail (View view) {
        String archivoPDF = "informePDF.pdf";
        asunto = "Informe PDF";
        mensaje = "Este fichero PDF ha sido creado en APP del teléfono móvil y enviado por correo electrónico";
        titulo = "Creación de informes en PDF";

        emailTo = edt_email.getText().toString();
        if(emailTo==null){
            emailTo = "micorreoelectronico@gmail.com";
        }

        Intent intentEmail = new Intent(Intent.ACTION_SEND);
        //para adjuntar el archivo PDF al email automáticamente
        Uri uri = Uri.fromFile(new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/",archivoPDF));
        intentEmail.putExtra(Intent.EXTRA_STREAM,uri);
        intentEmail.putExtra(Intent.EXTRA_EMAIL, new String[]{emailTo});
        //intentEmail.setData(Uri.parse("mailto:"));
        intentEmail.putExtra(Intent.EXTRA_TITLE,titulo);
        intentEmail.putExtra(Intent.EXTRA_SUBJECT,asunto);
        intentEmail.putExtra(Intent.EXTRA_TEXT,mensaje);
        //intentEmail.setType("text/plain");
        intentEmail.setType("application/pdf");
        //startActivity(intentEmail);

        if (emailTo!=null){
            startActivity(Intent.createChooser(intentEmail,"Gmail"));
            Toast toast = Toast.makeText(this,"Preparado para evío de Email...",Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER,0,0);
            toast.show();
        }else{
            Toast toast = Toast.makeText(this,"Email NO enviado",Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER,0,0);
            toast.show();
        }
    }
}