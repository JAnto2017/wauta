package com.agf.wauta;

import androidx.appcompat.app.AppCompatActivity;

import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class Temperaturas extends AppCompatActivity {

    private TextView tv1,tv2,tv3,tv4,tv5;
    private EditText edt_temperatura;
    private Spinner spinner;
    private String sDato;
    private float fDato;

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temperaturas);

        //fija vista en portrait evitar landscape
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //quitar la barra superior de la vista
        getSupportActionBar().hide();

        tv1 = findViewById(R.id.tv_02_temp);
        tv2 = findViewById(R.id.tv_04_temp);
        tv3 = findViewById(R.id.tv_06_temp);
        tv4 = findViewById(R.id.tv_08_temp);
        tv5 = findViewById(R.id.tv_10_temp);
        edt_temperatura = findViewById(R.id.edt_temperatura);

        //para el spinner asignación
        spinner = findViewById(R.id.spinner_temp);
        String[] opciones = {"Grado Celsius","Grado Fahrenheit","Kelvin","Grado Newton","Grado Rankine"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,R.layout.support_simple_spinner_dropdown_item,opciones);
        spinner.setAdapter(adapter);
    }
    //---------------------------------------------------------------------------------------------- método para evitar excepccion edit-text vacío
    /**
     * método para evitar excepccion edit-text vacío
     * @param _num
     * @return
     */
    public float checkNumero(String _num){
        float resultado = 0f;
        try {
            if (_num!=null){
                resultado = Float.parseFloat(_num);
            }
        }catch (NumberFormatException e){
            Toast.makeText(this,"Error campo vacío",Toast.LENGTH_SHORT).show();
        }
        return resultado;
    }
    //---------------------------------------------------------------------------------------------- método para determinar selección del spinner
    /**
     * método para determinar selección del spinner
     * @param view
     */
    public void seleccionarSpinner(View view){
        //activa sonido con click botón flotante
        MediaPlayer mp = MediaPlayer.create(this, R.raw.buttonsounduno);
        mp.start();

        //convertimos dato de entrada de string -> float
        sDato = edt_temperatura.getText().toString();
        fDato = checkNumero(sDato);

        String seleccion = spinner.getSelectedItem().toString();

        if (seleccion.equals("Grado Celsius")){
            if (fDato!=0f){
                calculaTemperaturas(1,fDato);
            }else{
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }else if (seleccion.equals("Grado Fahrenheit")){
            if (fDato!=0f){
                calculaTemperaturas(2,fDato);
            }else{
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }else if (seleccion.equals("Kelvin")){
            if (fDato!=0f){
                calculaTemperaturas(3,fDato);
            }else{
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }else if (seleccion.equals("Grado Newton")){
            if (fDato!=0f){
                calculaTemperaturas(4,fDato);
            }else{
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }else if (seleccion.equals("Grado Rankine")){
            if (fDato!=0f){
                calculaTemperaturas(5,fDato);
            }else{
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }
    }
    //---------------------------------------------------------------------------------------------- método de cálculo para las temperaturas
    /**
     * método de cálculo para las temperaturas
     * @param i
     * @param fDato
     */
    private void calculaTemperaturas(int i, float fDato) {
        switch (i){
            case 1: //ºC
                tv1.setText(String.valueOf(fDato));
                tv2.setText(String.valueOf((fDato*1.8)+32));
                tv3.setText(String.valueOf(fDato+273.15));
                tv4.setText(String.valueOf(fDato*0.33));
                tv5.setText(String.valueOf(fDato*493.47));
                break;
            case 2: //ºF
                tv1.setText(String.valueOf((fDato-32)/1.8));
                tv2.setText(String.valueOf(fDato));
                tv3.setText(String.valueOf((5/9)*(fDato-32)+273.15));
                tv4.setText(String.valueOf(fDato*(-5.683333)));
                tv5.setText(String.valueOf(fDato*460.67));
                break;
            case 3: //K
                tv1.setText(String.valueOf(fDato-273.15));
                tv2.setText(String.valueOf(1.8*(fDato-273.15)+32));
                tv3.setText(String.valueOf(fDato));
                tv4.setText(String.valueOf(fDato*(-89.8095)));
                tv5.setText(String.valueOf(fDato*1.8));
                break;
            case 4: //ºN
                tv1.setText(String.valueOf(fDato*3.030303030));
                tv2.setText(String.valueOf(fDato*37.454545454));
                tv3.setText(String.valueOf(fDato*276.180303030));
                tv4.setText(String.valueOf(fDato));
                tv5.setText(String.valueOf(fDato*497.124545454));
                break;
            case 5: //ºRa
                tv1.setText(String.valueOf(fDato*(-272.5944444)));
                tv2.setText(String.valueOf(fDato*(-458.67)));
                tv3.setText(String.valueOf(fDato*0.55555555555));
                tv4.setText(String.valueOf(fDato*(-89.95616666)));
                tv5.setText(String.valueOf(fDato));
                break;
        }
    }
}