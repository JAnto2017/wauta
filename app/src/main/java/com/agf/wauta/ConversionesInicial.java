package com.agf.wauta;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class ConversionesInicial extends AppCompatActivity {

    private TableRow tableRow_1, tableRow_2, tableRow_3, tableRow_4;
    private TextView tv_01,tv_02,tv_03,tv_04,tv_05,tv_06,tv_07,tv_08,tv_09,tv_10,tv_11,tv_12;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuestionario_inicial);

        tv_01 = findViewById(R.id.tv_cu_01);
        tv_02 = findViewById(R.id.tv_cu_02);
        tv_03 = findViewById(R.id.tv_cu_03);
        tv_04 = findViewById(R.id.tv_cu_04);
        tv_05 = findViewById(R.id.tv_cu_05);
        tv_06 = findViewById(R.id.tv_cu_06);
        tv_07 = findViewById(R.id.tv_cu_07);
        tv_08 = findViewById(R.id.tv_cu_08);
        tv_09 = findViewById(R.id.tv_cu_09);
        tv_10 = findViewById(R.id.tv_cu_10);
        tv_11 = findViewById(R.id.tv_cu_11);
        tv_12 = findViewById(R.id.tv_cu_12);
        tableRow_1 = findViewById(R.id.tableRow_1);
        tableRow_2 = findViewById(R.id.tableRow_2);
        tableRow_3 = findViewById(R.id.tableRow_3);
        tableRow_4 = findViewById(R.id.tableRow_4);

        //quitar la barra superior de la vista
        getSupportActionBar().hide();

        //establece animaciones por filas
        tableRow_1.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotar_dextrogiro));
        tableRow_2.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.izq_derecha));
        tableRow_3.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotar_levogiro));
        tableRow_4.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.dcha_izquierda));

        //sonido de la vista
        MediaPlayer mp = MediaPlayer.create(this, R.raw.sdalert20);
        mp.start();
    }

    //---------------------------------------------------------------------------------------------- métodos Áreas
    /**
     * métodos Áreas
     * @param view
     */
    public void Act_tv_1(View view){
        MediaPlayer mp = MediaPlayer.create(this,R.raw.buttonsounduno);
        mp.start();
        Toast.makeText(this,"Áreas",Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, Area.class);
        startActivity(intent);
    }
    //---------------------------------------------------------------------------------------------- métodos Energía
    /**
     * métodos Energía
     * @param view
     */
    public void Act_tv_2(View view){
        MediaPlayer mp = MediaPlayer.create(this,R.raw.buttonsounduno);
        mp.start();
        Toast.makeText(this, "Energías",Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, Energia.class);
        startActivity(intent);
    }
    //---------------------------------------------------------------------------------------------- métodos Longitud
    /**
     * métodos Longitud
     * @param view
     */
    public void Act_tv_3(View view){
        MediaPlayer mp = MediaPlayer.create(this,R.raw.buttonsounduno);
        mp.start();
        Toast.makeText(this, "Longitudes",Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, Longitud.class);
        startActivity(intent);
    }
    //---------------------------------------------------------------------------------------------- métodos de potencias
    /**
     * métodos de potencias
     * @param view
     */
    public void Act_tv_4(View view){
        MediaPlayer mp = MediaPlayer.create(this,R.raw.buttonsounduno);
        mp.start();
        Intent intent = new Intent(this, Potencias.class);
        startActivity(intent);
    }
    //---------------------------------------------------------------------------------------------- métodos de temperaturas
    /**
     * métodos de temperaturas
     * @param view
     */
    public void Act_tv_5(View view){
        MediaPlayer mp = MediaPlayer.create(this,R.raw.buttonsounduno);
        mp.start();
        Intent intent = new Intent(this, Temperaturas.class);
        startActivity(intent);
    }
    //---------------------------------------------------------------------------------------------- métodos de tiempos
    /**
     * métodos de tiempos
     * @param view
     */
    public void Act_tv_6(View view){
        MediaPlayer mp = MediaPlayer.create(this,R.raw.buttonsounduno);
        mp.start();
        Intent intent = new Intent(this, Tiempo.class);
        startActivity(intent);
    }
    //---------------------------------------------------------------------------------------------- métodos de volumen
    /**
     * métodos de volumen
     * @param view
     */
    public void Act_tv_7(View view){
        MediaPlayer mp = MediaPlayer.create(this,R.raw.buttonsounduno);
        mp.start();
        Intent intent = new Intent(this, Volumen.class);
        startActivity(intent);
    }
    //---------------------------------------------------------------------------------------------- métodos de ptesión
    /**
     * métodos de ptesión
     * @param view
     */
    public void Act_tv_8(View view){
        MediaPlayer mp = MediaPlayer.create(this,R.raw.buttonsounduno);
        mp.start();
        Intent intent = new Intent(this, Presion.class);
        startActivity(intent);
    }
    //---------------------------------------------------------------------------------------------- métodos de prefijos
    /**
     * métodos de prefijos
     * @param view
     */
    public void Act_tv_9(View view){
        MediaPlayer mp = MediaPlayer.create(this,R.raw.buttonsounduno);
        mp.start();
        Intent intent = new Intent(this, Prefijos.class);
        startActivity(intent);
    }
    //---------------------------------------------------------------------------------------------- métodos de tensión eléctrica
    /**
     * métodos de tensión eléctrica
     * @param view
     */
    public void Act_tv_10(View view){
        MediaPlayer mp = MediaPlayer.create(this,R.raw.buttonsounduno);
        mp.start();
        Intent intent = new Intent(this,TensionElectrica.class);
        startActivity(intent);
    }
    //---------------------------------------------------------------------------------------------- métodos de flujo luminoso
    /**
     * métodos de flujo luminoso
     * @param view
     */
    public void Act_tv_11(View view){
        MediaPlayer mp = MediaPlayer.create(this,R.raw.buttonsounduno);
        mp.start();
        Intent intent = new Intent(this, FlujoLuminoso.class);
        startActivity(intent);
    }
    //---------------------------------------------------------------------------------------------- métodos de flujo magnético
    /**
     * métodos de flujo magnético
     * @param view
     */
    public void Act_tv_12(View view){
        MediaPlayer mp = MediaPlayer.create(this,R.raw.buttonsounduno);
        mp.start();
        Intent intent = new Intent(this, FlujoMagnetico.class);
        startActivity(intent);
    }


}