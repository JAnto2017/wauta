package com.agf.wauta;

import androidx.appcompat.app.AppCompatActivity;

import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class Energia extends AppCompatActivity {

    private Spinner spinner;
    private TextView tv1,tv2,tv3,tv4,tv5,tv6,tv7;
    private EditText edt_energ;
    private String sDato;
    private float fDato;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_energia);

        //fija vista en portrait evitar landscape
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //quitar la barra superior de la vista
        getSupportActionBar().hide();

        tv1 = findViewById(R.id.tv_02_energ);
        tv2 = findViewById(R.id.tv_04_energ);
        tv3 = findViewById(R.id.tv_06_energ);
        tv4 = findViewById(R.id.tv_08_energ);
        tv5 = findViewById(R.id.tv_10_energ);
        tv6 = findViewById(R.id.tv_12_energ);
        tv7 = findViewById(R.id.tv_14_energ);
        edt_energ = findViewById(R.id.edt_energia);

        //para el spinner
        spinner = findViewById(R.id.spinner_energia);
        String[] opciones = {"Julio","Electrón-Voltio","MW·h","kW·h","W·h","Kcal","Caloría1"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item,opciones);
        spinner.setAdapter(adapter);

    }

    //---------------------------------------------------------------------------------------------- método para evitar excepccion edit-text vacío
    /**
     * método para evitar excepccion edit-text vacío
     * @param _num
     * @return
     */
    public float checkNumero(String _num){
        float resultado = 0f;
        try {
            if (_num!=null){
                resultado = Float.parseFloat(_num);
            }
        }catch (NumberFormatException e){
            Toast.makeText(this,"Error campo vacío",Toast.LENGTH_SHORT).show();
        }
        return resultado;
    }
    //---------------------------------------------------------------------------------------------- método para determinar selección del spinner
    /**
     * método para determinar selección del spinner
     * @param view
     */
    public void seleccSpinner(View view){
        //activa sonido con click botón flotante
        MediaPlayer mp = MediaPlayer.create(this, R.raw.buttonsounduno);
        mp.start();

        //convertimos dato de entrada de string -> float
        sDato = edt_energ.getText().toString();
        fDato = checkNumero(sDato);

        String seleccion = spinner.getSelectedItem().toString();

        if (seleccion.equals("Julio")){
            if (fDato!=0f){
                calculaEnergia(1,fDato);
            }else{
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }else if (seleccion.equals("Electrón-Voltio")){
            if (fDato!=0f){
                calculaEnergia(2,fDato);
            }else{
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }else if (seleccion.equals("MW·h")){
            if (fDato!=0f){
                calculaEnergia(3,fDato);
            }else {
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }else if (seleccion.equals("kW·h")){
            if (fDato!=0f){
                calculaEnergia(4,fDato);
            }else{
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }else if (seleccion.equals("W·h")){
            if (fDato!=0f){
                calculaEnergia(5,fDato);
            }else{
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }else if (seleccion.equals("Kcal")){
            if (fDato!=0f){
                calculaEnergia(6,fDato);
            }else{
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }else if (seleccion.equals("Caloría")){
            if (fDato!=0f){
                calculaEnergia(7,fDato);
            }else{
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }
    }
    //---------------------------------------------------------------------------------------------- método calcular energía
    /**
     * método calcular energía
     * Julio, Elec-Vol, MW·h, KW.h, W·h, kCal, cal
     * @param _i
     * @param _fDato
     */
    protected void calculaEnergia(int _i, float _fDato){
        switch (_i){
            case 1: //julio
                tv1.setText(String.valueOf(_fDato));
                tv2.setText(String.valueOf(_fDato*6.242e+18));
                tv3.setText(String.valueOf(_fDato*2.7778e-10));
                tv4.setText(String.valueOf(_fDato*2.7778e-7));
                tv5.setText(String.valueOf(_fDato*2.7778e-4));
                tv6.setText(String.valueOf(_fDato*0.000239006));
                tv7.setText(String.valueOf(_fDato*0.238846));
                break;
            case 2: //electrón-voltio
                tv1.setText(String.valueOf(_fDato*1.6e-19));
                tv2.setText(String.valueOf(_fDato));
                tv3.setText(String.valueOf(_fDato*4.4505e-29));
                tv4.setText(String.valueOf(_fDato*4.4505e-26));
                tv5.setText(String.valueOf(_fDato*4.4505e-23));
                tv6.setText(String.valueOf(_fDato*3.8267e-23));
                tv7.setText(String.valueOf(_fDato*3.8267e-20));
                break;
            case 3: //MW·h
                tv1.setText(String.valueOf(_fDato*3.6e+9));
                tv2.setText(String.valueOf(_fDato*2.246e+28));
                tv3.setText(String.valueOf(_fDato));
                tv4.setText(String.valueOf(_fDato*1000));
                tv5.setText(String.valueOf(_fDato*1.0e+6));
                tv6.setText(String.valueOf(_fDato*8.598e+5));
                tv7.setText(String.valueOf(_fDato*8.598e+8));
                break;
            case 4: //kW·h
                tv1.setText(String.valueOf(_fDato*3.6e+6));
                tv2.setText(String.valueOf(_fDato*2.2469e+25));
                tv3.setText(String.valueOf(_fDato*0.001));
                tv4.setText(String.valueOf(_fDato));
                tv5.setText(String.valueOf(_fDato*1000));
                tv6.setText(String.valueOf(_fDato*859.845));
                tv7.setText(String.valueOf(_fDato*8.598e+5));
                break;
            case 5: //W·h
                tv1.setText(String.valueOf(_fDato*3600));
                tv2.setText(String.valueOf(_fDato*2.2469e+22));
                tv3.setText(String.valueOf(_fDato*1.0e-6));
                tv4.setText(String.valueOf(_fDato*0.001));
                tv5.setText(String.valueOf(_fDato));
                tv6.setText(String.valueOf(_fDato*0.859845));
                tv7.setText(String.valueOf(_fDato*859.845));
                break;
            case 6: //kCal
                tv1.setText(String.valueOf(_fDato*4186.8));
                tv2.setText(String.valueOf(_fDato*2.613e+22));
                tv3.setText(String.valueOf(_fDato*1.163e-6));
                tv4.setText(String.valueOf(_fDato*0.001163));
                tv5.setText(String.valueOf(_fDato*1.163));
                tv6.setText(String.valueOf(_fDato));
                tv7.setText(String.valueOf(_fDato*1000));
                break;
            case 7: //caloría
                tv1.setText(String.valueOf(_fDato*4.1868));
                tv2.setText(String.valueOf(_fDato*2.613193e+19));
                tv3.setText(String.valueOf(_fDato*1.163e-9));
                tv4.setText(String.valueOf(_fDato*1.163e-6));
                tv5.setText(String.valueOf(_fDato*0.001163));
                tv6.setText(String.valueOf(_fDato*0.001));
                tv7.setText(String.valueOf(_fDato));
                break;
        }

    }
}