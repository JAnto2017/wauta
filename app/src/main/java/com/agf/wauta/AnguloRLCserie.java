package com.agf.wauta;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Paint;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetBehavior;

public class AnguloRLCserie extends AppCompatActivity {

    BottomSheetBehavior behavior;
    View vistaEmergente;
    private EditText edt_XL,edt_XC,edt_R;
    private TextView tv_salida, tv_titulo;
    private ImageView flecha;
    private String sXL, sXC, sR;
    private Float fXL, fXC, fR;
    private Double dAnguloSerie, dAnguloSerieSinDecim;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_angulo_rlcserie);

        //barra con flecha y quitar texto
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);

        //para el layout emergente
        vistaEmergente = findViewById(R.id.bottomSheet);        //Vista emergente
        behavior = BottomSheetBehavior.from(vistaEmergente);
        behavior.setState(BottomSheetBehavior.STATE_HIDDEN);    //inicial está oculta

        //titulo subrayado
        tv_titulo = findViewById(R.id.tv_1_anguloSerie);
        tv_titulo.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);

        edt_XL = findViewById(R.id.edt_1_anguloSerie);
        edt_XC = findViewById(R.id.edt_2_anguloSerie);
        edt_R = findViewById(R.id.edt_3_anguloSerie);
        tv_salida = findViewById(R.id.tv_5_anguloSerie);

        flecha = findViewById(R.id.img_flecha_rlc);
        flecha.setVisibility(View.INVISIBLE);
    }
    //---------------------------------------------------------------------------------------------- para mover flecha por activity al hacer click en edittext
    /**
     * para mover flecha por activity al hacer click en edittext
     * @param view
     */
    public void lanzarFlecha(View view){
        flecha.setVisibility(View.VISIBLE);
        flecha.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(),R.anim.flecha));

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                flecha.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade_suave));
            }
        },3900);
        flecha.setVisibility(View.INVISIBLE);
    }
    //---------------------------------------------------------------------------------------------- Para los tres botones barra navegación superior
    /**
     * Para los tres botones barra navegación superior
     * @param manu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu manu){
        getMenuInflater().inflate(R.menu.menuopcionescalculos, manu);
        return true;
    }
    //---------------------------------------------------------------------------------------------- Para los tres botones barra navegación superior
    /**
     * Para los tres botones barra navegación superior
     * @param menuItem
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem){

        if (menuItem.getItemId() != R.id.item1 && menuItem.getItemId() != R.id.item2 && menuItem.getItemId() != R.id.item3)
            onBackPressed();    //este es para la flecha izquierda del toolbar

        switch (menuItem.getItemId()){
            case R.id.item1:    //para calcular
                MediaPlayer mp1 = MediaPlayer.create(this,R.raw.buttonsounduno);
                mp1.start();
                calculoAnguloRLCserie();
                return true;
            case R.id.item2:    //para layout emergente
                MediaPlayer mp2 = MediaPlayer.create(this,R.raw.claroringtonestono);
                mp2.start();
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                return true;
            case R.id.item3:    //para limpiar
                Toast.makeText(getApplicationContext(),"Limpiando selección",Toast.LENGTH_SHORT).show();
                MediaPlayer mp3 = MediaPlayer.create(this,R.raw.sdalert20);
                mp3.start();
                limpiar();
                return true;
        }
        return true;
    }
    //---------------------------------------------------------------------------------------------- botón dentro de layout emergente
    /**
     * botón dentro de layout emergente
     * @param view
     */
    public void cerrarSheet(View view){
        behavior.setState(BottomSheetBehavior.STATE_HIDDEN);
    }
    //----------------------------------------------------------------------------------------------
    /**
     * Limpiar elementos
     */
    private void limpiar() {
        edt_XL.setText("");
        edt_XC.setText("");
        edt_R.setText("");
        tv_salida.setText("_");
    }
    //---------------------------------------------------------------------------------------------- método para evitar excepccion edit-text vacío
    /**
     * método para evitar excepccion edit-text vacío
     * @param _num
     * @return
     * @exception NumberFormatException
     */
    public float checkNumero(String _num){
        float resultado = 0.0f;
        try {
            if (_num!=null){
                resultado = Float.parseFloat(_num);
            }
        }catch (NumberFormatException e){
            tv_salida.setText("_");
            Toast mytoasErrot = Toast.makeText(getApplicationContext(),"<Error campo de entrada de datos vacío>",Toast.LENGTH_SHORT);
            mytoasErrot.setGravity(Gravity.CENTER_VERTICAL,0,0);
            mytoasErrot.show();
            MediaPlayer mp4 = MediaPlayer.create(this,R.raw.miedodos);
            mp4.start();
        }
        return resultado;
    }
    //----------------------------------------------------------------------------------------------
    /**
     * método de cálculo ángulo circuito RLC serie
     */
    private void calculoAnguloRLCserie() {
        sXL = edt_XL.getText().toString();
        fXL = checkNumero(sXL);
        sXC = edt_XC.getText().toString();
        fXC = checkNumero(sXC);
        sR = edt_R.getText().toString();
        fR = checkNumero(sR);

        if (fR != 0){
            dAnguloSerie = Math.toDegrees(Math.atan((fXL - fXC) / fR));
            dAnguloSerieSinDecim = Math.round(dAnguloSerie * 100d) /100d;
            activaAnimacion(dAnguloSerieSinDecim);
        }else {
            Toast.makeText(this,"El valor de R no puede ser 0",Toast.LENGTH_SHORT).show();
        }
    }
    //---------------------------------------------------------------------------------------------- método para activar animación de salida en TextView
    /**
     * método para activar animación de salida en TextView
     * @param _dato
     */
    public void activaAnimacion(Double _dato){
        tv_salida.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade_suave));
        tv_salida.setText(String.valueOf("φ = "+_dato) + " º");
    }

}