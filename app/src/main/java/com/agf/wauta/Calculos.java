package com.agf.wauta;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;

public class Calculos extends AppCompatActivity {

    MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculos);

        //quitar la barra superior de la vista
        getSupportActionBar().hide();
    }
    //---------------------------------------------------------------------------------------------- método para lanzar - POTENCIA ACTIVA
    /**
     * método para lanzar - POTENCIA ACTIVA
     * @param view
     */
    public void potenciaActiva(View view){
        //sonido al pulsar button
        mediaPlayer = MediaPlayer.create(this, R.raw.smsalertuno);
        mediaPlayer.start();

        //para lanzar la actividad PotenciaActiva
        Intent intent = new Intent(this, PotenciaActica.class);
        startActivity(intent);
    }
    //---------------------------------------------------------------------------------------------- método para lanzar - POTENCIA APARENTE
    /**
     * método para lanzar - POTENCIA APARENTE
     * @param view
     */
    public void potenciaAparente(View view){
        //sonido al pulsar button
        mediaPlayer = MediaPlayer.create(this, R.raw.smsalertuno);
        mediaPlayer.start();

        //para lanzar la actividad
        Intent intent = new Intent(this, PotenciaAparente.class);
        startActivity(intent);
    }
    //---------------------------------------------------------------------------------------------- método para lanzar - POTENCIA REACTIVA
    /**
     * método para lanzar - POTENCIA REACTIVA
     * @param view
     */
    public void potenicaReactiva(View view){
        //sonido al pulsar button
        mediaPlayer = MediaPlayer.create(this, R.raw.smsalertuno);
        mediaPlayer.start();

        //para lanzar la actividad
        Intent intent = new Intent(this, PotenciaReactiva.class);
        startActivity(intent);
    }
    //---------------------------------------------------------------------------------------------- método para lanzar - FACTOR DE POTENCIA
    /**
     * método para lanzar - FACTOR DE POTENCIA
     * @param view
     */
    public void factorPotencia(View view){
        //sonido al pulsar button
        mediaPlayer = MediaPlayer.create(this, R.raw.smsalertuno);
        mediaPlayer.start();

        //para lanzar la actividad a
        Intent intent = new Intent(this, FactorPotencia.class);
        startActivity(intent);
    }
    //---------------------------------------------------------------------------------------------- método para lanzar - RESISTENCIA DE UN CABLE
    /**
     * método para lanzar - RESISTENCIA DE UN CABLE
     * @param view
     */
    public void resistenciaConductor(View view){
        //sonido al pulsar button
        mediaPlayer = MediaPlayer.create(this, R.raw.smsalertuno);
        mediaPlayer.start();

        //para lanzar la actividad
        Intent intent = new Intent(this, ResistenciaConductor.class);
        startActivity(intent);
    }
    //---------------------------------------------------------------------------------------------- método para lanzar - DIVISOR DE TENSIÓN
    /**
     * método para lanzar - DIVISOR DE TENSIÓN
     * @param view
     */
    public void divisorTension(View view){
        //sonido al pulsar button
        mediaPlayer = MediaPlayer.create(this, R.raw.smsalertuno);
        mediaPlayer.start();

        //para lanzar la actividad
        Intent intent = new Intent(this, DivisorTension.class);
        startActivity(intent);
    }
    //---------------------------------------------------------------------------------------------- método para lanzar - RESISTENCIA DE UN LED
    /**
     * método para lanzar - RESISTENCIA DE UN LED
     * @param view
     */
    public void resistenciaLED(View view){
        //sonido al pulsar button
        mediaPlayer = MediaPlayer.create(this, R.raw.smsalertuno);
        mediaPlayer.start();

        //para lanzar la actividad
        Intent intent = new Intent(this, ResistenciaLED.class);
        startActivity(intent);
    }
    //---------------------------------------------------------------------------------------------- método para lanzar - FRECUENCIA
    /**
     * método para lanzar - FRECUENCIA
     * @param view
     */
    public void frecuencia(View view){
        //sonido al pulsar button
        mediaPlayer = MediaPlayer.create(this, R.raw.smsalertuno);
        mediaPlayer.start();

        //para lanzar la actividad
        Intent intent = new Intent(this, Frecuencia.class);
        startActivity(intent);
    }
    //---------------------------------------------------------------------------------------------- método para lanzar - PERIODO
    /**
     * método para lanzar - PERIODO
     * @param view
     */
    public void periodo(View view){
        //sonido al pulsar button
       mediaPlayer = MediaPlayer.create(this, R.raw.smsalertuno);
        mediaPlayer.start();

        //para lanzar la actividad
        Intent intent = new Intent(this, Periodo.class);
        startActivity(intent);
    }
    //---------------------------------------------------------------------------------------------- método para lanzar - LONGITUD DE ONDA
    /**
     * método para lanzar - LONGITUD DE ONDA
     * @param view
     */
    public void longitudOnda(View view){
        //sonido al pulsar button
        mediaPlayer = MediaPlayer.create(this, R.raw.smsalertuno);
        mediaPlayer.start();

        //para lanzar la actividad
        Intent intent = new Intent(this, LongitudOnda.class);
        startActivity(intent);
    }
    //---------------------------------------------------------------------------------------------- método para lanzar - RESISTENCIA EQUIVALENTE
    /**
     * método para lanzar - RESISTENCIA EQUIVALENTE
     * @param view
     */
    public void resistoresParalelo(View view){
        //sonido al pulsar button
        mediaPlayer = MediaPlayer.create(this, R.raw.smsalertuno);
        mediaPlayer.start();

        //para lanzar la actividad
        Intent intent = new Intent(this, ResistoresParalelo.class);
        startActivity(intent);
    }
    //---------------------------------------------------------------------------------------------- método para lanzar - POTENCIA (LEY DE JOULE)
    /**
     * método para lanzar - POTENCIA (LEY DE JOULE)
     * @param view
     */
    public void leyJoule(View view){
        //sonido al pulsar button
        mediaPlayer = MediaPlayer.create(this, R.raw.smsalertuno);
        mediaPlayer.start();

        //para lanzar la actividad
        Intent intent = new Intent(this, LeyJoule.class);
        startActivity(intent);
    }
    //---------------------------------------------------------------------------------------------- método para lanzar - INDUCCION MAGNETICA
    /**
     * método para lanzar - INDUCCION MAGNETICA
     * @param view
     */
    public void induccionMagnetica(View view){
        //sonido al pulsar button
        mediaPlayer = MediaPlayer.create(this, R.raw.smsalertuno);
        mediaPlayer.start();

        //para lanzar la actividad
        Intent intent = new Intent(this, InduccionMagnetica.class);
        startActivity(intent);
    }
    //---------------------------------------------------------------------------------------------- método para lanzar - FLUJO MAGNÉTICO
    /**
     * método para lanzar - FLUJO MAGNÉTICO
     * @param view
     */
    public void flujoMagnetico(View view){
        //sonido al pulsar button
        mediaPlayer = MediaPlayer.create(this, R.raw.smsalertuno);
        mediaPlayer.start();

        //para lanzar la actividad
        Intent intent = new Intent(this, CalcularFlujoMagnetico.class);
        startActivity(intent);
    }
    //---------------------------------------------------------------------------------------------- método para lanzar - COEFICIENTE DE AUTOINDUCCION
    /**
     * método para lanzar - COEFICIENTE DE AUTOINDUCCION
     * @param view
     */
    public void  coeficienteAutoinducc(View view){
        //sonido al pulsar button
        mediaPlayer = MediaPlayer.create(this, R.raw.smsalertuno);
        mediaPlayer.start();

        //para lanzar la actividad
        Intent intent = new Intent(this, CoefAutoinduccion.class);
        startActivity(intent);
    }
    //---------------------------------------------------------------------------------------------- método para lanzar - REACTANCIA INDUCTIVA
    /**
     * método para lanzar - REACTANCIA INDUCTIVA
     * @param view
     */
    public void reactanciaInductiva(View view){
        //sonido al pulsar button
        mediaPlayer = MediaPlayer.create(this, R.raw.smsalertuno);
        mediaPlayer.start();

        //para lanzar la actividad
        Intent intent = new Intent(this, ReactanciaInductiva.class);
        startActivity(intent);
    }
    //---------------------------------------------------------------------------------------------- método para lanzar - REACTANCIA CAPACITIVA
    /**
     * método para lanzar - REACTANCIA CAPACITIVA
     * @param view
     */
    public void reactanciaCapacitiva(View view){
        //sonido al pulsar button
        mediaPlayer = MediaPlayer.create(this, R.raw.smsalertuno);
        mediaPlayer.start();

        //para lanzar la actividad
        Intent intent = new Intent(this, ReactanciaCapacitiva.class);
        startActivity(intent);
    }
    //---------------------------------------------------------------------------------------------- método para lanzar - MODULO RLC SERIE
    /**
     * método para lanzar - MODULO RLC SERIE
     * @param view
     */
    public void moduloRLCserie(View view){
        //sonido al pulsar button
        mediaPlayer = MediaPlayer.create(this, R.raw.smsalertuno);
        mediaPlayer.start();

        //para lanzar la actividad
        Intent intent = new Intent(this, ModuloRLCserie.class);
        startActivity(intent);
    }
    //---------------------------------------------------------------------------------------------- método para lanzar - ÁNGULO RLC SERIE
    /**
     * método para lanzar - ÁNGULO RLC SERIE
     * @param view
     */
    public void anguloRLCserie(View view){
        //sonido al pulsar button
        mediaPlayer = MediaPlayer.create(this, R.raw.smsalertuno);
        mediaPlayer.start();

        //para lanzar la actividad
        Intent intent = new Intent(this, AnguloRLCserie.class);
        startActivity(intent);
    }
    //---------------------------------------------------------------------------------------------- método para lanzar - FRECUENCIA DE RESONANCIA
    /**
     * método para lanzar - FRECUENCIA DE RESONANCIA
     * @param view
     */
    public void frecuResonanciaRLCserie(View view){
        //sonido al pulsar button
        mediaPlayer = MediaPlayer.create(this, R.raw.smsalertuno);
        mediaPlayer.start();

        //para lanzar la actividad
        Intent intent = new Intent(this, FrecuenciaResonancia.class);
        startActivity(intent);
    }
    //---------------------------------------------------------------------------------------------- método para lanzar - FACTOR DE POTENCIA A CORREGIR
    /**
     * método para lanzar - FACTOR DE POTENCIA A CORREGIR
     * @param view
     */
    public void correccionFactorPot(View view){
        //sonido al pulsar button
        mediaPlayer = MediaPlayer.create(this, R.raw.smsalertuno);
        mediaPlayer.start();

        //para lanzar la actividad
        Intent intent = new Intent(this, CorreccionFactorPotencia.class);
        startActivity(intent);
    }
    //---------------------------------------------------------------------------------------------- método para lanzar - FRECUENCIA CORTE FILTRO
    /**
     * método para lanzar - FRECUENCIA CORTE FILTRO
     * @param view
     */
    public void frecCorteFiltro(View view){
        //sonido al pulsar button
        mediaPlayer = MediaPlayer.create(this, R.raw.smsalertuno);
        mediaPlayer.start();

        //para lanzar la actividad
        Intent intent = new Intent(this, FrecuenciaCorteFiltro.class);
        startActivity(intent);
    }
    //----------------------------------------------------------------------------------------------

}