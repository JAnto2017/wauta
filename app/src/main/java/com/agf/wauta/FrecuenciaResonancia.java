package com.agf.wauta;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Paint;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetBehavior;

public class FrecuenciaResonancia extends AppCompatActivity {

    BottomSheetBehavior behavior;
    View vistaEmergente;
    private EditText edt_L,edt_C;
    private TextView tv_salida, tv_titulo;
    private String sL,sC;
    private ImageView flecha;
    private float fL, fC;
    private double dFrecRes,dFrecResSinDedim;

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frecuencia_resonancia);

        //barra con flecha y quitar texto
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);

        //para el layout emergente
        vistaEmergente = findViewById(R.id.bottomSheet);        //Vista emergente
        behavior = BottomSheetBehavior.from(vistaEmergente);
        behavior.setState(BottomSheetBehavior.STATE_HIDDEN);    //inicial está oculta

        //titulo subrayado
        tv_titulo = findViewById(R.id.tv_1_frecResonancia);
        tv_titulo.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);

        edt_L = findViewById(R.id.edt_1_frecResonancia);
        edt_C = findViewById(R.id.edt_2_frecResonancia);
        tv_salida = findViewById(R.id.tv_4_freResonancia);

        flecha = findViewById(R.id.img_flecha_fr);
        flecha.setVisibility(View.INVISIBLE);
    }
    //---------------------------------------------------------------------------------------------- para flecha moverse en activity al hacer click en edittext
    /**
     * para flecha moverse en activity al hacer click en edittext
     * @param view
     */
    public void activaFlecha(View view){
        flecha.setVisibility(View.VISIBLE);
        flecha.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.flecha));

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                flecha.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_suave));
            }
        },3500);
        flecha.setVisibility(View.INVISIBLE);
    }
    //---------------------------------------------------------------------------------------------- Para los tres botones barra navegación superior
    /**
     * Para los tres botones barra navegación superior
     * @param manu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu manu){
        getMenuInflater().inflate(R.menu.menuopcionescalculos, manu);
        return true;
    }
    //---------------------------------------------------------------------------------------------- Para los tres botones barra navegación superior
    /**
     * Para los tres botones barra navegación superior
     * @param menuItem
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem){

        if (menuItem.getItemId() != R.id.item1 && menuItem.getItemId() != R.id.item2 && menuItem.getItemId() != R.id.item3)
            onBackPressed();    //este es para la flecha izquierda del toolbar

        switch (menuItem.getItemId()){
            case R.id.item1:    //para calcular
                MediaPlayer mp1 = MediaPlayer.create(this,R.raw.buttonsounduno);
                mp1.start();
                calcularFrecResonancia();
                return true;
            case R.id.item2:    //para layout emergente
                MediaPlayer mp2 = MediaPlayer.create(this,R.raw.claroringtonestono);
                mp2.start();
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                return true;
            case R.id.item3:    //para limpiar
                Toast.makeText(getApplicationContext(),"Limpiando selección",Toast.LENGTH_SHORT).show();
                MediaPlayer mp3 = MediaPlayer.create(this,R.raw.sdalert20);
                mp3.start();
                limpiar();
                return true;
        }
        return true;
    }
    //---------------------------------------------------------------------------------------------- botón dentro de layout emergente~
    /**
     * botón dentro de layout emergente~
     * @param view
     */
    public void cerrarSheet(View view){
        behavior.setState(BottomSheetBehavior.STATE_HIDDEN);
    }
    //----------------------------------------------------------------------------------------------
    /**
     * método para limpiar los contenidos de los EditText y TextView
     */
    private void limpiar() {
        edt_L.setText("");
        edt_C.setText("");
        tv_salida.setText("_");
    }
    //---------------------------------------------------------------------------------------------- método para evitar excepccion edit-text vacío
    /**
     * método para evitar excepccion edit-text vacío
     * @param _num
     * @return
     */
    public float checkNumero(String _num){
        float resultado = 0.0f;
        try {
            if (_num!=null){
                resultado = Float.parseFloat(_num);
            }
        }catch (NumberFormatException e){
            tv_salida.setText("_");
            Toast mytoasErrot = Toast.makeText(getApplicationContext(),"<Error campo de entrada de datos vacío>",Toast.LENGTH_SHORT);
            mytoasErrot.setGravity(Gravity.CENTER_VERTICAL,0,0);
            mytoasErrot.show();
            MediaPlayer mp4 = MediaPlayer.create(this,R.raw.miedodos);
            mp4.start();
        }
        return resultado;
    }
    //----------------------------------------------------------------------------------------------
    /**
     * Método para calcular la frecuencia de resonancia
     */
    private void calcularFrecResonancia() {
        sL = edt_L.getText().toString();
        fL = checkNumero(sL);
        sC = edt_C.getText().toString();
        fC = checkNumero(sC);

        if (fL != 0f && fC != 0f){
            dFrecRes = 1 / (Math.PI * 2 * Math.sqrt(fL * fC));
            dFrecResSinDedim = Math.round(dFrecRes * 100d) / 100d;
            activaAnimacion(dFrecResSinDedim);
        }else{
            Toast.makeText(this,"Valor del Condensador y Bobina NO pueden ser 0",Toast.LENGTH_SHORT).show();
        }
    }
    //---------------------------------------------------------------------------------------------- método para activar animación de salida en TextView
    /**
     * método para activar animación de salida en TextView
     * @param _dato
     */
    public void activaAnimacion(Double _dato){
        tv_salida.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade_suave));
        tv_salida.setText(String.valueOf("F = "+_dato) + " Hz");
    }

}