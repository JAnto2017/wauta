package com.agf.wauta;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Paint;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetBehavior;

public class FrecuenciaCorteFiltro extends AppCompatActivity {

    BottomSheetBehavior behavior;
    View vistaEmergente;
    private TextView tv_titulo, tv_salida;
    private Spinner spinner;
    private final String[] seleccFiltro = {"Filtro Pasa Bajo","Filtro Pasa Alto"};
    private EditText edt_R1,edt_C1;
    private ImageView imagen, flecha;
    private String sR1, sC1;
    private float fR1, fC1, frecCorteFil;

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frecuencia_corte_filtro);

        //barra con flecha y quitar texto
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);

        //para el layout emergente
        vistaEmergente = findViewById(R.id.bottomSheet);        //Vista emergente
        behavior = BottomSheetBehavior.from(vistaEmergente);
        behavior.setState(BottomSheetBehavior.STATE_HIDDEN);    //inicial está oculta

        tv_titulo = findViewById(R.id.tv_1_frecCorteFiltro);    //permite subrayar el texto
        tv_titulo.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);

        spinner = findViewById(R.id.sp_frecCorteFiltro);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,R.layout.support_simple_spinner_dropdown_item,seleccFiltro);
        spinner.setAdapter(adapter);

        edt_R1 = findViewById(R.id.edt_1_frecCorteFiltro);
        edt_C1 = findViewById(R.id.edt_2_frecCorteFiltro);
        tv_salida = findViewById(R.id.tv_5_frecCorteFiltro);

        imagen = findViewById(R.id.img_frecCorteFiltro);
        imagen.setVisibility(View.INVISIBLE);

        flecha = findViewById(R.id.id_img_flecha_fc);
        flecha.setVisibility(View.INVISIBLE);
    }
    //---------------------------------------------------------------------------------------------- para mover imagen de flecha por vista al hacer click en Edit
    /**
     * para mover imagen de flecha por vista al hacer click en Edit
     * @param view
     */
    public void activarFlecha(View view){
        flecha.setVisibility(View.VISIBLE);
        flecha.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.flecha));

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                flecha.setX(1100.0f);
                flecha.setY(90.0f);
                flecha.setRotation(-55);
                //flecha.setVerticalScrollbarPosition(20);
                flecha.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_suave));
            }
        },3500);
        flecha.setVisibility(View.INVISIBLE);
    }
    //---------------------------------------------------------------------------------------------- Para los tres botones barra navegación superior
    /**
     * Para los tres botones barra navegación superior
     * @param manu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu manu){
        getMenuInflater().inflate(R.menu.menuopcionescalculos, manu);
        return true;
    }
    //---------------------------------------------------------------------------------------------- Para los tres botones barra navegación superior
    /**
     * Para los tres botones barra navegación superior
     * @param menuItem
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem){

        if (menuItem.getItemId() != R.id.item1 && menuItem.getItemId() != R.id.item2 && menuItem.getItemId() != R.id.item3)
            onBackPressed();    //este es para la flecha izquierda del toolbar

        switch (menuItem.getItemId()){
            case R.id.item1:    //para calcular
                MediaPlayer mp1 = MediaPlayer.create(this,R.raw.buttonsounduno);
                mp1.start();
                calcularFrecuenCorteFiltro();
                return true;
            case R.id.item2:    //para layout emergente
                MediaPlayer mp2 = MediaPlayer.create(this,R.raw.claroringtonestono);
                mp2.start();
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                return true;
            case R.id.item3:    //para limpiar
                Toast.makeText(getApplicationContext(),"Limpiando selección",Toast.LENGTH_SHORT).show();
                MediaPlayer mp3 = MediaPlayer.create(this,R.raw.sdalert20);
                mp3.start();
                limpiar();
                return true;
        }
        return true;
    }
    //---------------------------------------------------------------------------------------------- botón dentro de layout emergente~
    /**
     * botón dentro de layout emergente~
     * @param view
     */
    public void cerrarSheet(View view){
        behavior.setState(BottomSheetBehavior.STATE_HIDDEN);
    }
    //----------------------------------------------------------------------------------------------
    /**
     * limpiar los EditText
     */
    private void limpiar() {
        edt_R1.setText("");
        edt_C1.setText("");
        tv_salida.setText("_");
        imagen.setVisibility(View.INVISIBLE);
    }
    //---------------------------------------------------------------------------------------------- método para evitar excepccion edit-text vacío
    /**
     * método para evitar excepccion edit-text vacío
     * @param _num
     * @return
     */
    public float checkNumero(String _num){
        float resultado = 0.0f;
        try {
            if (_num!=null){
                resultado = Float.parseFloat(_num);
            }
        }catch (NumberFormatException e){
            tv_salida.setText("_");
            Toast mytoasErrot = Toast.makeText(getApplicationContext(),"<Error campo de entrada de datos vacío>",Toast.LENGTH_SHORT);
            mytoasErrot.setGravity(Gravity.CENTER_VERTICAL,0,0);
            mytoasErrot.show();
            MediaPlayer mp4 = MediaPlayer.create(this,R.raw.miedodos);
            mp4.start();
        }
        return resultado;
    }
    //---------------------------------------------------------------------------------------------- método principal de cálculo
    /**
     * método principal de cálculo
     */
    private void calcularFrecuenCorteFiltro() {

        sR1 = edt_R1.getText().toString();
        fR1 = checkNumero(sR1);
        sC1 = edt_C1.getText().toString();
        fC1 = checkNumero(sC1);

        String seleccion = spinner.getSelectedItem().toString();

        if (seleccion.equals("Filtro Pasa Bajo")){

            if (fR1==0f || fC1==0f){
                Toast.makeText(this,"El Condensador y/o Resistencia NO pueden ser 0",Toast.LENGTH_SHORT).show();
            }else{
                frecCorteFil = calculo(fR1,fC1);
                activaAnimacion(frecCorteFil);
                imagen.setImageResource(R.drawable.filtropasobajo);
                imagen.setVisibility(View.VISIBLE);
            }
        }else if (seleccion.equals("Filtro Pasa Alto")){

            if (fR1==0f || fC1==0f){
                Toast.makeText(this,"El Condensador y/o Resistencia NO pueden ser 0",Toast.LENGTH_SHORT).show();
            }else{
                frecCorteFil = calculo(fR1,fC1);
                activaAnimacion(frecCorteFil);
                imagen.setImageResource(R.drawable.filtropasoalto);
                imagen.setVisibility(View.VISIBLE);
            }
        }
    }
    //----------------------------------------------------------------------------------------------
    /**
     * método de cálculo frecuencia de corte del filtro
     * @param fR1
     * @param fC1
     * @return
     */
    private float calculo(float fR1, float fC1) {
        float resultado;
        resultado = (float) (1 / (Math.PI * 2 * fR1 * fC1));
        return resultado;
    }
    //---------------------------------------------------------------------------------------------- método para activar animación de salida en TextView
    /**
     * método para activar animación de salida en TextView
     * @param _dato
     */
    public void activaAnimacion(Float _dato){
        tv_salida.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade_suave));
        tv_salida.setText(String.valueOf("F = "+_dato) + " Hz");
    }
}