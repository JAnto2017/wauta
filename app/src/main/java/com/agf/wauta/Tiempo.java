package com.agf.wauta;

import androidx.appcompat.app.AppCompatActivity;

import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class Tiempo extends AppCompatActivity {

    private TextView tv1,tv2,tv3,tv4,tv5,tv6;
    private EditText edt_tiempo;
    private Spinner spinner;
    private String sDato;
    private float fDato;

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tiempo);

        //fija vista en portrait evitar landscape
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //quitar la barra superior de la vista
        getSupportActionBar().hide();

        tv1 = findViewById(R.id.tv_02_tiempo);
        tv2 = findViewById(R.id.tv_04_tiempo);
        tv3 = findViewById(R.id.tv_06_tiempo);
        tv4 = findViewById(R.id.tv_08_tiempo);
        tv5 = findViewById(R.id.tv_10_tiempo);
        tv6 = findViewById(R.id.tv_12_tiempo);
        edt_tiempo = findViewById(R.id.edt_tiempo);

        //para el spinner
        spinner = findViewById(R.id.spinner_tiempo);
        String[] opciones = {"Año","Mes","Días","Horas","Minutos","Segundos"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item,opciones);
        spinner.setAdapter(adapter);
    }
    //---------------------------------------------------------------------------------------------- método para evitar excepccion edit-text vacío

    /**
     * método para evitar excepccion edit-text vacío
     * @param _num
     * @return
     */
    public float checkNumero(String _num){
        float resultado = 0f;
        try {
            if (_num!=null){
                resultado = Float.parseFloat(_num);
            }
        }catch (NumberFormatException e){
            Toast.makeText(this,"Error campo vacío",Toast.LENGTH_SHORT).show();
        }
        return resultado;
    }
    //---------------------------------------------------------------------------------------------- método para determinar selección del spinner

    /**
     * método para determinar selección del spinner
     * @param view
     */
    public void seleccionSpinner(View view){
        //activa sonido con click botón flotante
        MediaPlayer mp = MediaPlayer.create(this, R.raw.buttonsounduno);
        mp.start();

        //convertimos dato de entrada de string -> float
        sDato = edt_tiempo.getText().toString();
        fDato = checkNumero(sDato);

        String seleccion = spinner.getSelectedItem().toString();

        if (seleccion.equals("Año")){
            if (fDato!=0f){
                calculaTiempos(1,fDato);
            }else {
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }else if (seleccion.equals("Mes")){
            if (fDato!=0f){
                calculaTiempos(2,fDato);
            }else {
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }else if (seleccion.equals("Días")){
            if (fDato!=0f){
                calculaTiempos(3,fDato);
            }else {
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }else if (seleccion.equals("Horas")){
            if (fDato!=0f){
                calculaTiempos(4,fDato);
            }else {
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }else if (seleccion.equals("Minutos")){
            if (fDato!=0f){
                calculaTiempos(5,fDato);
            }else {
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }else if (seleccion.equals("Segundos")){
            if (fDato!=0f){
                calculaTiempos(6,fDato);
            }else {
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }
    }
    //----------------------------------------------------------------------------------------------

    /**
     * método para el cálculo conversión de tiempos
     * @param i
     * @param fDato
     */
    private void calculaTiempos(int i, float fDato) {
        switch (i){
            case 1: //año
                tv1.setText(String.valueOf(fDato));
                tv2.setText(String.valueOf(fDato*12.0002099099));
                tv3.setText(String.valueOf(fDato*365.2425));
                tv4.setText(String.valueOf(fDato*8765.82));
                tv5.setText(String.valueOf(fDato*5.259492e+5));
                tv6.setText(String.valueOf(fDato*3.155695e+7));
                break;
            case 2: //mes
                tv1.setText(String.valueOf(fDato*0.083331875));
                tv2.setText(String.valueOf(fDato));
                tv3.setText(String.valueOf(fDato*30.436342592));
                tv4.setText(String.valueOf(fDato*730.472222));
                tv5.setText(String.valueOf(fDato*4.382833e+4));
                tv6.setText(String.valueOf(fDato*2.6297e+6));
                break;
            case 3: //días
                tv1.setText(String.valueOf(fDato*0.003));
                tv2.setText(String.valueOf(fDato*0.033));
                tv3.setText(String.valueOf(fDato));
                tv4.setText(String.valueOf(fDato*24));
                tv5.setText(String.valueOf(fDato*1440));
                tv6.setText(String.valueOf(fDato*8.64e+4));
                break;
            case 4: //horas
                tv1.setText(String.valueOf(fDato*0.0001));
                tv2.setText(String.valueOf(fDato*0.00137));
                tv3.setText(String.valueOf(fDato*0.0417));
                tv4.setText(String.valueOf(fDato));
                tv5.setText(String.valueOf(fDato*60));
                tv6.setText(String.valueOf(fDato*3600));
                break;
            case 5: //minutos
                tv1.setText(String.valueOf(fDato*1.9e-9));
                tv2.setText(String.valueOf(fDato*2.2816e-5));
                tv3.setText(String.valueOf(fDato*0.00069));
                tv4.setText(String.valueOf(fDato*0.0167));
                tv5.setText(String.valueOf(fDato));
                tv6.setText(String.valueOf(fDato*60));
                break;
            case 6: //segundos
                tv1.setText(String.valueOf(fDato*3.168e-8));
                tv2.setText(String.valueOf(fDato*3.802e-7));
                tv3.setText(String.valueOf(fDato*1.157e-5));
                tv4.setText(String.valueOf(fDato*0.00028));
                tv5.setText(String.valueOf(fDato*0.0167));
                tv6.setText(String.valueOf(fDato));
                break;
        }
    }
}