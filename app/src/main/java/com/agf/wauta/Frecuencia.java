package com.agf.wauta;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Paint;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetBehavior;

public class Frecuencia extends AppCompatActivity {

    BottomSheetBehavior behavior;
    View vistaEmergente;
    private EditText edt_periodo;
    private TextView tv_salida, tv_titulo;
    private String sPeriodo;
    private Float fPeriodo, fFrecuencia;

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frecuencia);

        //barra con flecha y quitar texto
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);

        //para el layout emergente
        vistaEmergente = findViewById(R.id.bottomSheet);        //Vista emergente
        behavior = BottomSheetBehavior.from(vistaEmergente);
        behavior.setState(BottomSheetBehavior.STATE_HIDDEN);    //inicial está oculta

        //titulo subrayado
        tv_titulo = findViewById(R.id.tv_1_frecuencia);
        tv_titulo.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);

        edt_periodo = findViewById(R.id.edt_1_frecuencia);
        tv_salida = findViewById(R.id.tv_3_frecuecnia);
    }
    //---------------------------------------------------------------------------------------------- Para los tres botones barra navegación superior
    /**
     * Para los tres botones barra navegación superior
     * @param manu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu manu){
        getMenuInflater().inflate(R.menu.menuopcionescalculos, manu);
        return true;
    }
    //---------------------------------------------------------------------------------------------- Para los tres botones barra navegación superior
    /**
     * Para los tres botones barra navegación superior
     * @param menuItem
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem){

        if (menuItem.getItemId() != R.id.item1 && menuItem.getItemId() != R.id.item2 && menuItem.getItemId() != R.id.item3)
            onBackPressed();    //este es para la flecha izquierda del toolbar

        switch (menuItem.getItemId()){
            case R.id.item1:    //para calcular
                MediaPlayer mp1 = MediaPlayer.create(this,R.raw.buttonsounduno);
                mp1.start();
                calcularFrecuencia();
                return true;
            case R.id.item2:    //para layout emergente
                MediaPlayer mp2 = MediaPlayer.create(this,R.raw.claroringtonestono);
                mp2.start();
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                return true;
            case R.id.item3:    //para limpiar
                Toast.makeText(getApplicationContext(),"Limpiando selección",Toast.LENGTH_SHORT).show();
                MediaPlayer mp3 = MediaPlayer.create(this,R.raw.sdalert20);
                mp3.start();
                limpiar();
                return true;
        }
        return true;
    }
    //---------------------------------------------------------------------------------------------- botón dentro de layout emergente~
    /**
     * botón dentro de layout emergente~
     * @param view
     */
    public void cerrarSheet(View view){
        behavior.setState(BottomSheetBehavior.STATE_HIDDEN);
    }
    //---------------------------------------------------------------------------------------------- para limpiar los editText y TextView
    /**
     * para limpiar los editText y TextView
     */
    private void limpiar() {
        edt_periodo.setText("");
        tv_salida.setText("_");
    }
    //---------------------------------------------------------------------------------------------- método para evitar excepccion edit-text vacío
    /**
     * método para evitar excepccion edit-text vacío
     * @param _num
     * @return
     */
    public float checkNumero(String _num){
        float resultado = 0.0f;
        try {
            if (_num!=null){
                resultado = Float.parseFloat(_num);
            }
        }catch (NumberFormatException e){
            tv_salida.setText("_");
            Toast mytoasErrot = Toast.makeText(getApplicationContext(),"<Error campo de entrada de datos vacío>",Toast.LENGTH_SHORT);
            mytoasErrot.setGravity(Gravity.CENTER_VERTICAL,0,0);
            mytoasErrot.show();
            MediaPlayer mp4 = MediaPlayer.create(this,R.raw.miedodos);
            mp4.start();
        }
        return resultado;
    }
    //---------------------------------------------------------------------------------------------- calcular la frecuencia
    /**
     * calcular la frecuencia
     */
    private void calcularFrecuencia() {
        sPeriodo = edt_periodo.getText().toString();
        fPeriodo = checkNumero(sPeriodo);

        if (fPeriodo != 0f){
            fFrecuencia = 1 / fPeriodo;
        }else{
            Toast miToastDiv0 = Toast.makeText(Frecuencia.this,"El periodo, NO puede ser 0",Toast.LENGTH_SHORT);
            miToastDiv0.setGravity(Gravity.CENTER_VERTICAL,0,0);
            miToastDiv0.show();
        }
        //para determinar si campos están vacío no represetar valor NULL en layout
        if (fFrecuencia==null){
            tv_salida.setText("_");
        }else{
            activaAnimacion(fFrecuencia);
        }
    }
    //---------------------------------------------------------------------------------------------- método para activar animación de salida en TextView
    /**
     * método para activar animación de salida en TextView
     * @param _dato
     */
    public void activaAnimacion(Float _dato){
        tv_salida.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade_suave));
        tv_salida.setText(String.valueOf("F = "+_dato) + " Hz");
    }
}