package com.agf.wauta;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import android.widget.Toolbar;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Cuestionarios extends AppCompatActivity {

    private RadioButton rb_p1_b,rb_p2_a,rb_p3_c,rb_p4_b,rb_p5_c,rb_p6_b;
    private RadioButton rb_p7_c,rb_p8_c,rb_p9_c,rb_p10_b,rb_p11_c;
    private RadioGroup rg_1,rg_2,rg_3,rg_4,rg_5,rg_6,rg_7,rg_8,rg_9,rg_10,rg_11;
    private int aciertos, errores;
    private String email = "ejemplo123@gmail.com";
    private String titulo;
    private String asunto;
    private String mensaje;
    private String fecha;
    private String resultados;
    private String FileName ="myFile";

    //----------------------------------------------------------------------------------------------
    /**
     * Método onCreate principal
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuestionarios);

        //barra con flecha y quitar texto
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        //getSupportActionBar().hide(); //inicialmente estará oculta

        //componentes RB asociado a las soluciones - las q son ciertas -
        rb_p1_b = findViewById(R.id.rb_preg1_a2);
        rb_p2_a = findViewById(R.id.rb_preg2_b1);
        rb_p3_c = findViewById(R.id.rb_preg3_c3);
        rb_p4_b = findViewById(R.id.rb_preg4_d2);
        rb_p5_c = findViewById(R.id.rb_preg5_e3);
        rb_p6_b = findViewById(R.id.rb_preg6_f2);
        rb_p7_c = findViewById(R.id.rb_preg7_g3);
        rb_p8_c = findViewById(R.id.rb_preg8_h3);
        rb_p9_c = findViewById(R.id.rb_preg9_i3);
        rb_p10_b= findViewById(R.id.rb_preg10_j2);
        rb_p11_c= findViewById(R.id.rb_preg11_k3);
        rg_1 = findViewById(R.id.rg_preg_1);
        rg_2 = findViewById(R.id.rg_preg_2);
        rg_3 = findViewById(R.id.rg_preg_3);
        rg_4 = findViewById(R.id.rg_preg_4);
        rg_5 = findViewById(R.id.rg_preg_5);
        rg_6 = findViewById(R.id.rg_preg_6);
        rg_7 = findViewById(R.id.rg_preg_7);
        rg_8 = findViewById(R.id.rg_preg_8);
        rg_9 = findViewById(R.id.rg_preg_9);
        rg_10= findViewById(R.id.rg_preg_10);
        rg_11= findViewById(R.id.rg_preg_11);

        //inicialización
        setAciertos(0);
        setErrores(0);

        //instancia formato de fecha
        //SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        //fecha = sdf.format(new Date());
    }
    //----------------------------------------------------------------------------------------------
    /**
     * Método para crear los menús de botones bara superior (menuopciones.xml)
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menuopciones,menu);
        return true;
    }
    //---------------------------------------------------------------------------------------------- acciones 3 botones en toolbar
    /**
     * acciones de las acciones menú desplegable (menuopciones.xml)
     * @param menuItem
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem){

        if (menuItem.getItemId() != R.id.opcion1 && menuItem.getItemId() != R.id.opcion2 && menuItem.getItemId() != R.id.opcion3 && menuItem.getItemId() != R.id.opcion4 && menuItem.getItemId() != R.id.opcion5)
            onBackPressed();    //este es para la flecha de la toolbar

        switch (menuItem.getItemId()){
            case R.id.opcion1:
                //Toast.makeText(this,"Realizando comprobaciones...",Toast.LENGTH_SHORT).show();
                MediaPlayer.create(this,R.raw.sdalert4).start();
                comprobaciones();
                return true;

            case R.id.opcion2:
                //Toast.makeText(this,"Enviando email...",Toast.LENGTH_SHORT).show();
                MediaPlayer.create(this,R.raw.claroringtonestono).start();
                enviarEmail();
                return true;

            case R.id.opcion3:
                Toast.makeText(this,"Limpiar seleccionados...",Toast.LENGTH_SHORT).show();
                MediaPlayer.create(this,R.raw.sdalert20).start();
                limpieza();
                return true;

            case R.id.opcion4:
                MediaPlayer.create(this,R.raw.sdalert4).start();
                guardarResultados();
                return true;

            case R.id.opcion5:
                MediaPlayer.create(this,R.raw.claroringtonestono).start();
                visualizarResultados();
                return true;
        }
        return true;
    }
    //----------------------------------------------------------------------------------------------
    /**
     * método usado para guardar datos de los resultados y de la fecha de realización
     */
    private void guardarResultados() {

        //para guardar la fecha y la hora
        fecha = java.text.DateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime());

        resultados = fecha+" | Aciertos: "+String.valueOf(getAciertos())+" Errores: "+String.valueOf(getErrores());

        SharedPreferences sharedPreferences = getSharedPreferences(FileName, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString("clave",resultados);
        editor.commit();

        Toast.makeText(Cuestionarios.this,"Datos guardados",Toast.LENGTH_SHORT).show();
    }
    //----------------------------------------------------------------------------------------------
    /**
     * método para visualizar los datos guardados en un SharedPreferences
     */
    private void visualizarResultados() {

        SharedPreferences sharedPreferences = getSharedPreferences(FileName,Context.MODE_PRIVATE);
        String sr = sharedPreferences.getString("clave","valor por defecto");

        Toast visualToast = Toast.makeText(Cuestionarios.this,sr,Toast.LENGTH_LONG);
        visualToast.setGravity(Gravity.CENTER,0,0);
        visualToast.show();
    }
    //---------------------------------------------------------------------------------------------- limpiar las selecciones
    /**
     * limpiar las selecciones
     */
    public void limpieza() {
        rg_1.clearCheck();
        rg_2.clearCheck();
        rg_3.clearCheck();
        rg_4.clearCheck();
        rg_5.clearCheck();
        rg_6.clearCheck();
        rg_7.clearCheck();
        rg_8.clearCheck();
        rg_9.clearCheck();
        rg_10.clearCheck();
        rg_11.clearCheck();

        setAciertos(0);
        setErrores(0);

        finish();
        startActivity(getIntent());
    }
    //---------------------------------------------------------------------------------------------- lógica de comprobación
    /**
     * lógica de comprobación
     */
    public void comprobaciones (){
        if (rb_p1_b.isChecked()){
            aciertos+=1;
            rb_p1_b.setBackgroundColor(Color.rgb(60,224,16));
        }else{
            errores+=1;
        }
        if (rb_p2_a.isChecked()){
            aciertos+=1;
            rb_p2_a.setBackgroundColor(Color.rgb(60,224,16));
        }else{
            errores+=1;
        }
        if (rb_p3_c.isChecked()){
            aciertos+=1;
            rb_p3_c.setBackgroundColor(Color.rgb(60,224,16));
        }else{
            errores+=1;
        }
        if (rb_p4_b.isChecked()){
            aciertos+=1;
            rb_p4_b.setBackgroundColor(Color.rgb(60,224,16));
        }else{
            errores+=1;
        }
        if (rb_p5_c.isChecked()){
            aciertos+=1;
            rb_p5_c.setBackgroundColor(Color.rgb(60,224,16));
        }else {
            errores+=1;
        }
        if (rb_p6_b.isChecked()){
            aciertos+=1;
            rb_p6_b.setBackgroundColor(Color.rgb(60,224,16));
        }else {
            errores+=1;
        }
        if (rb_p7_c.isChecked()){
            aciertos+=1;
            rb_p7_c.setBackgroundColor(Color.rgb(60,224,16));
        }else {
            errores+=1;
        }
        if (rb_p8_c.isChecked()){
            aciertos+=1;
            rb_p8_c.setBackgroundColor(Color.rgb(60,224,16));
        }else{
            errores+=1;
        }
        if (rb_p9_c.isChecked()){
            aciertos+=1;
            rb_p9_c.setBackgroundColor(Color.rgb(60,224,16));
        }else {
            errores+=1;
        }
        if (rb_p10_b.isChecked()){
            aciertos+=1;
            rb_p10_b.setBackgroundColor(Color.rgb(60,224,16));
        }else {
            errores+=1;
        }
        if (rb_p11_c.isChecked()){
            aciertos+=1;
            rb_p11_c.setBackgroundColor(Color.rgb(60,224,16));
        }else {
            errores+=1;
        }

        setAciertos(aciertos);
        setErrores(errores);
        Toast toast = Toast.makeText(this,"Aciertos: "+getAciertos()+" Errores: "+getErrores(),Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER,0,0);
        toast.show();
    }
    //---------------------------------------------------------------------------------------------- Email
    /**
     * Email
     */
    public void enviarEmail() {
        titulo = "Test";
        asunto = "Test de repaso";
        mensaje = "Estos son los Aciertos: "+getAciertos()+" y los Errores: "+getErrores()+" obtenidos tras la realización del Test de repaso.";
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_EMAIL,new String[]{email});
        intent.putExtra(Intent.EXTRA_TITLE,titulo);
        intent.putExtra(Intent.EXTRA_SUBJECT,asunto);
        intent.putExtra(Intent.EXTRA_TEXT,mensaje);
        //establecemos tipo de Intent
        intent.setType("message/rfc822");
        startActivity(Intent.createChooser(intent,"Gmail"));
        Toast.makeText(this,"Enviado email",Toast.LENGTH_SHORT).show();
    }
    //----------------------------------------------------------------------------------------------
    /**
     * métodos setter & getters
     * @param aciertos
     */
    public void setAciertos(int aciertos) {
        this.aciertos = aciertos;
    }

    public void setErrores(int errores) {
        this.errores = errores;
    }

    public int getAciertos() {
        return aciertos;
    }

    public int getErrores() {
        return errores;
    }
}