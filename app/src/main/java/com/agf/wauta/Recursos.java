package com.agf.wauta;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class Recursos extends AppCompatActivity {

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recursos);

        //quitar la barra superior de la vista
        getSupportActionBar().hide();
    }

    //---------------------------------------------------------------------------------------------- WEB
    /**
     * método para visualizar la WEB
     * @param v
     */
    public void metAnim_web (View v) {

        //sonido 'tinuninuuu' al hacer click
        MediaPlayer mp = MediaPlayer.create(this,R.raw.smsalertuno);
        mp.start();
        //Toast toast = Toast.makeText(this,"WEB",Toast.LENGTH_SHORT);
        //toast.show();
        Intent intent = new Intent(this, Web.class);
        startActivity(intent);
    }
    //---------------------------------------------------------------------------------------------- PDF
    /**
     * método para PDF
     * @param v
     */
    public void metAnim_pdf (View v) {
        //sonido al hacer click
        MediaPlayer mp = MediaPlayer.create(this,R.raw.smsalerttres);
        mp.start();

        //Toast toast = Toast.makeText(this,"PDF",Toast.LENGTH_SHORT);
        //toast.show();
        Intent intentPdf = new Intent(this, PdfCrea.class);
        startActivity(intentPdf);
    }
    //---------------------------------------------------------------------------------------------- SENSOR
    /**
     * método para SENSOR
     * @param v
     */
    public void metAnim_sensor (View v) {
        //sonido '' al hacer click
        //MediaPlayer mp = MediaPlayer.create(this,R.raw.smsalertdos);
        //mp.start();

        //Toast toast = Toast.makeText(this,"SENSOR",Toast.LENGTH_SHORT);
        //toast.show();
        Intent intentSen = new Intent(this, SensorLuz.class);
        startActivity(intentSen);
    }


}