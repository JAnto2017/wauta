package com.agf.wauta;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Paint;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetBehavior;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class CorreccionFactorPotencia extends AppCompatActivity {

    BottomSheetBehavior behavior;
    View vistaEmergente;
    private TextView tv_titulo, tv_salida_1, tv_salida_2, tv_salida_3;
    private EditText edt_fdpCorregir, edt_fdpDeseado, edt_Potencia;
    private ImageView flecha;
    private String sFdpCorr, sFdpDese, sPoten;
    private Double dFdpCorr, dFdpDese, dPoten;
    //---------------------------------------------------------------------------------------------- onCreate
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_correccion_factor_potencia);

        //barra con flecha y quitar texto
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);

        //para el layout emergente
        vistaEmergente = findViewById(R.id.bottomSheet);        //Vista emergente
        behavior = BottomSheetBehavior.from(vistaEmergente);
        behavior.setState(BottomSheetBehavior.STATE_HIDDEN);    //inicial está oculta

        tv_titulo = findViewById(R.id.tv_1_cFactorPotencia);    //permite subrayar el texto
        tv_titulo.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);

        tv_salida_1 = findViewById(R.id.tv_5_cFactorPotencia);
        tv_salida_2 = findViewById(R.id.tv_6_cFactorPotencia);
        tv_salida_3 = findViewById(R.id.tv_7_cFactorPotencia);

        edt_fdpCorregir = findViewById(R.id.edt_1_cFactorPotencia);
        edt_fdpDeseado = findViewById(R.id.edt_2_cFactorPotencia);
        edt_Potencia = findViewById(R.id.edt_3_cFactorPotencia);

        flecha = findViewById(R.id.img_flecha);
        flecha.setVisibility(View.INVISIBLE);
    }
    //---------------------------------------------------------------------------------------------- método para activar flecha al pulsar EditText
    /**
     * método para activar flecha al pulsar EditText
     * @param view
     */
    public void activarFlecha(View view){
        flecha.setVisibility(View.VISIBLE);
        flecha.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(),R.anim.flecha));

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                flecha.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade_suave));
            }
        }, 2500);

        flecha.setVisibility(View.INVISIBLE);
    }
    //---------------------------------------------------------------------------------------------- Para los tres botones barra navegación superior
    /**
     * Para los tres botones barra navegación superior
     * @param manu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu manu){
        getMenuInflater().inflate(R.menu.menuopcionescalculos, manu);
        return true;
    }
    //---------------------------------------------------------------------------------------------- Para los tres botones barra navegación superior
    /**
     * Para los tres botones barra navegación superior
     * @param menuItem
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem){

        if (menuItem.getItemId() != R.id.item1 && menuItem.getItemId() != R.id.item2 && menuItem.getItemId() != R.id.item3)
            onBackPressed();    //este es para la flecha izquierda del toolbar

        switch (menuItem.getItemId()){
            case R.id.item1:    //para calcular
                MediaPlayer mp1 = MediaPlayer.create(this,R.raw.buttonsounduno);
                mp1.start();
                calcularFactorPotencia();
                return true;
            case R.id.item2:    //para layout emergente
                MediaPlayer mp2 = MediaPlayer.create(this,R.raw.claroringtonestono);
                mp2.start();
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                return true;
            case R.id.item3:    //para limpiar
                Toast.makeText(getApplicationContext(),"Limpiando selección",Toast.LENGTH_SHORT).show();
                MediaPlayer mp3 = MediaPlayer.create(this,R.raw.sdalert20);
                mp3.start();
                limpiar();
                return true;
        }
        return true;
    }
    //---------------------------------------------------------------------------------------------- botón dentro de layout emergente~
    /**
     * botón dentro de layout emergente~
     * @param view
     */
    public void cerrarSheet(View view){
        behavior.setState(BottomSheetBehavior.STATE_HIDDEN);
    }
    //----------------------------------------------------------------------------------------------
    /**
     * limpiar todo contenido de los EditText
     */
    private void limpiar() {
        edt_fdpCorregir.setText("");
        edt_fdpDeseado.setText("");
        edt_Potencia.setText("");
        tv_salida_1.setText(getResources().getString(R.string.tv_5_cFactorPotencia));
        tv_salida_2.setText(getResources().getString(R.string.tv_6_cFactorPotencia));
        tv_salida_3.setText(getResources().getString(R.string.tv_7_cFactorPotencia));
    }
    //---------------------------------------------------------------------------------------------- método para evitar excepccion edit-text vacío
    /**
     * método para evitar excepccion edit-text vacío
     * @param _num
     * @return
     */
    public double checkNumero(String _num){
        double resultado = 0.0d;
        try {
            if (_num!=null){
                resultado = Double.parseDouble(_num);
            }
        }catch (NumberFormatException e){

            Toast mytoasErrot = Toast.makeText(getApplicationContext(),"<Error campo de entrada de datos vacío>",Toast.LENGTH_SHORT);
            mytoasErrot.setGravity(Gravity.CENTER_VERTICAL,0,0);
            mytoasErrot.show();
            MediaPlayer mp4 = MediaPlayer.create(this,R.raw.miedodos);
            mp4.start();
        }
        return resultado;
    }
    //---------------------------------------------------------------------------------------------- método de calculo principal
    /**
     * método de calculo principal del factor de potencia
     */
    private void calcularFactorPotencia() {
        sFdpCorr = edt_fdpCorregir.getText().toString();
        dFdpCorr = checkNumero(sFdpCorr);
        sFdpDese = edt_fdpDeseado.getText().toString();
        dFdpDese = checkNumero(sFdpDese);
        sPoten = edt_Potencia.getText().toString();
        dPoten = checkNumero(sPoten);

        double ang_1,ang_2;
        ang_1 = Math.acos(dFdpCorr);
        ang_2 = Math.acos(dFdpDese);

        double dCondMono = calculoCondensadorMonofasico(ang_1,ang_2,dPoten);
        activaAnimacion_1(dCondMono);

        new Handler().postDelayed(new Runnable(){
            public void run() {
                double dCondTriEst = calculoCondensadorTrifEstrella(1, ang_1,ang_2,dPoten);
                activaAnimacion_2(dCondTriEst);
            }
        }, 5000);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                double dCondTriTri = calculoCondensadorTrifEstrella(2, ang_1,ang_2,dPoten);
                activaAnimacion_3(dCondTriTri);
            }
        }, 10000);

    }
    //---------------------------------------------------------------------------------------------- cálculo del condesador en trifásico conexión estrella
    /**
     * cálculo del condesador en trifásico conexión estrella
     * @param _selector
     * @param _ang_1
     * @param _ang_2
     * @param _dPoten
     * @return
     */
    protected double calculoCondensadorTrifEstrella(int _selector, double _ang_1, double _ang_2, Double _dPoten) {
        double resultCtri = 0.0d;
        double pot_1 = 0.0d, pot_2 = 0.0d;

        pot_1 = _dPoten * (Math.tan(_ang_1) - Math.tan(_ang_2));
        pot_2 = pot_1 / 3;

        if (_selector == 1){
            resultCtri = pot_2 / (Math.PI * 2 * 50 * Math.pow(230,2));
        }
        if (_selector == 2){
            resultCtri = pot_2 / (Math.PI * 2 * 50 * Math.pow(400,2));
        }

        return resultCtri;
    }

    //---------------------------------------------------------------------------------------------- cálculo del condensador en monofásico
    /**
     * cálculo del condensador en monofásico
     * @param _ang_1
     * @param _ang_2
     * @param _dPoten
     * @return
     */
    protected double calculoCondensadorMonofasico(double _ang_1, double _ang_2, double _dPoten) {
        double resultado=0.0d, resulSinDeci=0.0d;

        resultado = (_dPoten * (Math.tan(_ang_1) - Math.tan(_ang_2))) / (Math.PI * 2 * 50 * Math.pow(230,2));
        //resulSinDeci = Math.round(resultado * 100d) / 100d;

        return resultado;
    }
    //---------------------------------------------------------------------------------------------- método para activar animación de salida en TextView
    public void activaAnimacion_1(Double _dato_1){
        tv_salida_1.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade_suave));
        tv_salida_1.setText(String.valueOf("C(230V) = "+_dato_1) + " F");
    }
    //----------------------------------------------------------------------------------------------
    public void activaAnimacion_2(Double _dato_2){
        tv_salida_2.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade_suave));
        tv_salida_2.setText(String.valueOf("C(400V Y) = "+_dato_2) + " F");
    }
    public void activaAnimacion_3(Double _dato_3){
        tv_salida_3.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade_suave));
        tv_salida_3.setText(String.valueOf("C(400V ∆) = "+_dato_3) + " F");
    }
}