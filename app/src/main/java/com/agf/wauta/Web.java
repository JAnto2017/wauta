package com.agf.wauta;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.webkit.WebView;

public class Web extends AppCompatActivity {
    /**
     * método que visualiza la página web
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);

        //quitar la barra superior de la vista
        getSupportActionBar().hide();

        //web
        WebView webView = (WebView) findViewById(R.id.webview_01);
        //setContentView(webView);
        webView.loadUrl("http://agf-hdg.tech/");
    }
}