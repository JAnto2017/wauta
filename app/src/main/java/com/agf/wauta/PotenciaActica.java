package com.agf.wauta;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Paint;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetBehavior;

public class PotenciaActica extends AppCompatActivity {

    BottomSheetBehavior behavior;
    View  vistaEmergente;
    private RadioButton rb1,rb2,rb3;
    private RadioGroup rg_potAct;
    private TextView tv_salidaDato, tv_titulo;
    private EditText edt_voltio, edt_corriente, edt_factor;
    private String sVoltio, sConrriente, sFactorPot;
    private Float fVoltio, fCorriente, fPotencia, fFactorPot;

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_potencia_actica);

        //barra con flecha y quitar texto
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);

        //para el layout emergente
        vistaEmergente = findViewById(R.id.bottomSheet);        //Vista emergente
        //layout_ = findViewById(R.id.layoutPrincipal);           //vista principal
        behavior = BottomSheetBehavior.from(vistaEmergente);
        behavior.setState(BottomSheetBehavior.STATE_HIDDEN);    //inicial está oculta

        //titulo subrayado
        tv_titulo = findViewById(R.id.tv_potenciaActiva_tit);
        tv_titulo.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);

        rg_potAct = findViewById(R.id.rg_potenciaActiva);
        rb1 = findViewById(R.id.rb_1_potenciaActiva);
        rb2 = findViewById(R.id.rb_2_potenciaActiva);
        rb3 = findViewById(R.id.rb_3_potenciaActiva);
        tv_salidaDato = findViewById(R.id.tv_03_potenciaActiva);
        edt_voltio = findViewById(R.id.edt_01_potenciaActiva);
        edt_corriente = findViewById(R.id.edt_02_potenciaActiva);
        edt_factor = findViewById(R.id.edt_04_potenciaActiva);
    }
    //----------------------------------------------------------------------------------------------
    /**
     * para menú contextual
     * @param manu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu manu){
        getMenuInflater().inflate(R.menu.menuopcionescalculos, manu);
        return true;
    }
    //---------------------------------------------------------------------------------------------- Para los tres botones barra navegación superior
    /**
     * Para los tres botones barra navegación superior
     * @param menuItem
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem){

        if (menuItem.getItemId() != R.id.item1 && menuItem.getItemId() != R.id.item2 && menuItem.getItemId() != R.id.item3)
            onBackPressed();    //este es para la flecha de la toolbar

        switch (menuItem.getItemId()){
            case R.id.item1:    //para calcular
                //Toast.makeText(this,"Realizando cálculos...",Toast.LENGTH_SHORT).show();
                MediaPlayer mp1 = MediaPlayer.create(this,R.raw.buttonsounduno);
                mp1.start();
                calculaPotenciaActiva();
                return true;
            case R.id.item2:    //para layout emergente
                //Toast.makeText(this,"Item 2 seleccionado",Toast.LENGTH_SHORT).show();
                MediaPlayer mp2 = MediaPlayer.create(this,R.raw.claroringtonestono);
                mp2.start();
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                return true;
            case R.id.item3:    //para limpiar
                Toast.makeText(this,"Limpiando selección",Toast.LENGTH_SHORT).show();
                MediaPlayer mp3 = MediaPlayer.create(this,R.raw.sdalert20);
                mp3.start();
                limpiar();
                return true;
        }
        return true;
    }
    //---------------------------------------------------------------------------------------------- método para limpiar la vista
    /**
     * método para limpiar la vista
     */
    public void limpiar(){
        rg_potAct.clearCheck();
        edt_corriente.setText("");
        edt_voltio.setText("");
        edt_factor.setText("");
    }
    //---------------------------------------------------------------------------------------------- método para evitar excepccion edit-text vacío
    /**
     * método para evitar excepccion edit-text vacío
     * @param _num
     * @return
     */
    public float checkNumero(String _num){
        float resultado = 0.0f;
        try {
            if (_num!=null){
                resultado = Float.parseFloat(_num);
            }
        }catch (NumberFormatException e){
            Toast mytoasErrot = Toast.makeText(this,"Error campo vacío",Toast.LENGTH_LONG);
            mytoasErrot.setGravity(Gravity.CENTER,0,0);
            mytoasErrot.show();
            MediaPlayer mp4 = MediaPlayer.create(this,R.raw.miedodos);
            mp4.start();
        }
        return resultado;
    }
    //---------------------------------------------------------------------------------------------- método para calacular la potencia
    /**
     * método para calacular la potencia
     */
    private void calculaPotenciaActiva() {

        if (rb1.isChecked()){
            //opción a 230 V
            sConrriente = edt_corriente.getText().toString();
            fCorriente = checkNumero(sConrriente);
            sFactorPot = edt_factor.getText().toString();
            fFactorPot = checkNumero(sFactorPot);
            fPotencia = fCorriente * 230 * fFactorPot;
            activaAnimacion(fPotencia);
        }else if (rb2.isChecked()){
            //opción a 400 V
            sConrriente = edt_corriente.getText().toString();
            fCorriente = checkNumero(sConrriente);
            sFactorPot = edt_factor.getText().toString();
            fFactorPot = checkNumero(sFactorPot);
            fPotencia = fCorriente * 400 * fFactorPot;
            activaAnimacion(fPotencia);
        }else if (rb3.isChecked()){
            //opción libre
            sConrriente = edt_corriente.getText().toString();
            fCorriente = checkNumero(sConrriente);
            sVoltio = edt_voltio.getText().toString();
            fVoltio = checkNumero(sVoltio);
            sFactorPot = edt_factor.getText().toString();
            fFactorPot = checkNumero(sFactorPot);
            fPotencia = fVoltio * fCorriente * fFactorPot;
            activaAnimacion(fPotencia);
        }else {
            Toast myToast = Toast.makeText(PotenciaActica.this,"Debes seleccionar una opción",Toast.LENGTH_LONG);
            myToast.setGravity(Gravity.CENTER,0,0);
            myToast.show();
        }
    }
    //---------------------------------------------------------------------------------------------- método para activar animación de salida en TextView
    /**
     * método para activar animación de salida en TextView
     * @param _potencia
     */
    public void activaAnimacion(Float _potencia){
        tv_salidaDato.setText(String.valueOf(_potencia) + " W");
        tv_salidaDato.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade_suave));
    }
    //---------------------------------------------------------------------------------------------- botón dentro de layout emergente~
    /**
     * botón dentro de layout emergente~
     * @param view
     */
    public void cerrarSheet(View view){
        behavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        //layout_.setBackgroundColor(getResources().getColor(R.color.white));
    }
}