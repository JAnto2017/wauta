package com.agf.wauta;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.media.MediaParser;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;

/**
 * @author Jose Antonio Delgado
 * @version 08/02/2022
 */
public class MainActivity extends AppCompatActivity {

    private TextView tv_1,tv_2,tv_3,tv_4;
    private Dialog dialog;
    private View view;
    //speech to text
    private final int ReqCode = 100;
    private String palabraRecibida;

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tv_1 = findViewById(R.id.tv_a1_1);
        tv_2 = findViewById(R.id.tv_a1_2);
        tv_3 = findViewById(R.id.tv_a1_3);
        tv_4 = findViewById(R.id.tv_a1_4);

        //animaciones desplazamientos
        tv_1.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(),R.anim.izq_derecha));
        tv_3.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(),R.anim.izq_derecha));
        tv_2.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(),R.anim.dcha_izquierda));
        tv_4.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(),R.anim.dcha_izquierda));
        //sonido de la actividad
        MediaPlayer mp = MediaPlayer.create(this,R.raw.smsalertcuatro);
        mp.start();
        //quitar la barra superior de la vista
        getSupportActionBar().hide();
    }
    //---------------------------------------------------------------------------------------------- click en animación para mostrar menú emergente

    /**
     * Método para crear ventana Dialog emergente en MainActivity al pulsar sobre animación
     * @param view
     */
    public void menEmerg(View view){

        dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_alert_dialog);
        dialog.setCancelable(true);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        Button btn_dialog = dialog.findViewById(R.id.btn_dialog);
        btn_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });
        dialog.show();
        //Toast.makeText(this,"prueba",Toast.LENGTH_SHORT).show();
    }

    //---------------------------------------------------------------------------------------------- Lanzar Activity CÁLCULOS

    /**
     * Activity CÁLCULOS
     * @param view
     */
    public void calculos(View view) {
        Intent intent = new Intent(this,Calculos.class);
        startActivity(intent);
        MediaPlayer mp1 = MediaPlayer.create(this, R.raw.buttonsounduno);
        mp1.start();
    }
    //---------------------------------------------------------------------------------------------- Lanzar Activity CONVERSIONES INICIAL

    /**
     * Activity CONVERSIONES INICIAL
     * @param view
     */
    public void conversiones(View view) {
        Intent intent = new Intent(this, ConversionesInicial.class);
        startActivity(intent);
        MediaPlayer mp2 = MediaPlayer.create(this,R.raw.buttonsounduno);
        mp2.start();
    }
    //---------------------------------------------------------------------------------------------- Lanzar Activity RECURSOS

    /**
     * Activity RECURSOS
     * @param view
     */
    public void recursos(View view) {
        Intent intent = new Intent(this, Recursos.class);
        startActivity(intent);
        MediaPlayer mp3 = MediaPlayer.create(this, R.raw.buttonsounduno);
        mp3.start();
    }
    //---------------------------------------------------------------------------------------------- Lanzar Activity CUESTIONARIOS

    /**
     * Activity CUESTIONARIOS
     * @param view
     */
    public void cuestionarios(View view){
        Intent intent = new Intent(this, Cuestionarios.class);
        startActivity(intent);
        MediaPlayer mp4 = MediaPlayer.create(this, R.raw.buttonsounduno);
        mp4.start();
    }
    //---------------------------------------------------------------------------------------------- Speech to text para la Activity en Landscape

    /**
     * speech to text
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (ReqCode){
            case ReqCode: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    palabraRecibida = result.get(0);
                    switch (palabraRecibida) {
                        case "cálculos":
                            Toast.makeText(this,"Has dicho CÁLCULOS",Toast.LENGTH_SHORT).show();
                            Intent intent0 = new Intent(this,Calculos.class);
                            retardos();
                            startActivity(intent0);
                            break;
                        case "conversiones":
                            Toast.makeText(this,"Has dicho CONVERSIONES",Toast.LENGTH_SHORT).show();
                            Intent intent1 = new Intent(this,ConversionesInicial.class);
                            retardos();
                            startActivity(intent1);
                            break;
                        case "recursos":
                            Toast.makeText(this,"Has dicho RECURSOS",Toast.LENGTH_SHORT).show();
                            Intent intent2 = new Intent(this,Recursos.class);
                            retardos();
                            startActivity(intent2);
                            break;
                        case "cuestionarios":
                            Toast.makeText(this,"Has dicho CUESTIONARIOS",Toast.LENGTH_SHORT).show();
                            Intent intent3 = new Intent(this,Cuestionarios.class);
                            retardos();
                            startActivity(intent3);
                            break;
                        default:
                            Toast.makeText(this,"Debes leer una de las opciones",Toast.LENGTH_LONG).show();
                    }
                }
            }
        }
    }
    //---------------------------------------------------------------------------------------------- speech to text

    /**
     * método para activar con el botón imagen de micrófono
     * @param view
     */
    public void speechToText(View view) {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_RESULTS, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,"Tienes que hablar");

        try {
            startActivityForResult(intent,ReqCode);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(getApplicationContext(),"Dispositivo no soportado",Toast.LENGTH_SHORT).show();
        }
    }
    //---------------------------------------------------------------------------------------------- retardo antes de abrir Activity

    /**
     * método usado para retardar la apertura de la activity una vez se ha solicitado
     * speech to text
     */
    public void retardos() {
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            Toast.makeText(this,"Se ha producido una Excepción",Toast.LENGTH_SHORT).show();
        }
    }
}