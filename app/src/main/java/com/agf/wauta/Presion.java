package com.agf.wauta;

import androidx.appcompat.app.AppCompatActivity;

import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class Presion extends AppCompatActivity {

    private TextView tv1,tv2,tv3,tv4,tv5,tv6;
    private EditText edt_presion;
    private Spinner spinner;
    private String sDato;
    private float fDato;

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_presion);

        //fija vista en portrait evitar landscape
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //quitar la barra superior de la vista
        getSupportActionBar().hide();

        tv1 = findViewById(R.id.tv_02_presion);
        tv2 = findViewById(R.id.tv_04_presion);
        tv3 = findViewById(R.id.tv_06_presion);
        tv4 = findViewById(R.id.tv_08_presion);
        tv5 = findViewById(R.id.tv_10_presion);
        tv6 = findViewById(R.id.tv_12_presion);
        edt_presion = findViewById(R.id.edt_presion);

        //para las opciones del spinner
        spinner = findViewById(R.id.spinner_presion);
        String[] opciones ={"Bar","Atmósfera (Atm)","Hectopascal (hPa)","kN/cm\u00B2","mmH20","mmHg"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item,opciones);
        spinner.setAdapter(adapter);
    }
    //---------------------------------------------------------------------------------------------- método para evitar excepccion edit-text vacío
    /**
     * método para evitar excepccion edit-text vacío
     * @param _num
     * @return
     */
    public float checkNumero(String _num){
        float resultado = 0f;
        try {
            if (_num!=null){
                resultado = Float.parseFloat(_num);
            }
        }catch (NumberFormatException e){
            Toast.makeText(this,"Error campo vacío",Toast.LENGTH_SHORT).show();
        }
        return resultado;
    }
    //---------------------------------------------------------------------------------------------- método para determinar selección del spinner
    /**
     * método para determinar selección del spinner
     * @param view
     */
    public void seleccionarSpinner(View view){
        //activa sonido con click botón flotante
        MediaPlayer mp = MediaPlayer.create(this, R.raw.buttonsounduno);
        mp.start();

        //convertimos dato de entrada de string -> float
        sDato = edt_presion.getText().toString();
        fDato = checkNumero(sDato);

        String seleccion = spinner.getSelectedItem().toString();

        if (seleccion.equals("Bar")){
            if (fDato!=0f){
                calculaPresion(1,fDato);
            }else{
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }else if (seleccion.equals("Atmósfera (Atm)")){
            if (fDato!=0f){
                calculaPresion(2,fDato);
            }else{
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }else if (seleccion.equals("Hectopascal (hPa)")){
            if (fDato!=0f){
                calculaPresion(3,fDato);
            }else{
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }else if (seleccion.equals("kN/cm2")){
            if (fDato!=0f){
                calculaPresion(4,fDato);
            }else{
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }else if (seleccion.equals("mmH20")){
            if (fDato!=0f){
                calculaPresion(5,fDato);
            }else{
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }else if (seleccion.equals("mmHg")){
            if (fDato!=0f){
                calculaPresion(6,fDato);
            }else{
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }
    }
    //---------------------------------------------------------------------------------------------- método para calcular
    /**
     * método para calcular
     * @param i
     * @param fDato
     */
    private void calculaPresion(int i, float fDato) {
        switch (i){
            case 1:
                tv1.setText(String.valueOf(fDato));
                tv2.setText(String.valueOf(fDato*0.987));
                tv3.setText(String.valueOf(fDato*1000));
                tv4.setText(String.valueOf(fDato*0.01));
                tv5.setText(String.valueOf(fDato*1.019e+4));
                tv6.setText(String.valueOf(fDato*750.0615));
                break;
            case 2:
                tv1.setText(String.valueOf(fDato*1.01325));
                tv2.setText(String.valueOf(fDato));
                tv3.setText(String.valueOf(fDato*1013.25));
                tv4.setText(String.valueOf(fDato*0.0101325));
                tv5.setText(String.valueOf(fDato*1.03325e+4));
                tv6.setText(String.valueOf(fDato*759.999819));
                break;
            case 3:
                tv1.setText(String.valueOf(fDato*0.001));
                tv2.setText(String.valueOf(fDato*0.0009869));
                tv3.setText(String.valueOf(fDato));
                tv4.setText(String.valueOf(fDato*1.0e-5));
                tv5.setText(String.valueOf(fDato*10.197));
                tv6.setText(String.valueOf(fDato*0.750));
                break;
            case 4:
                tv1.setText(String.valueOf(fDato*100));
                tv2.setText(String.valueOf(fDato*98.692));
                tv3.setText(String.valueOf(fDato*1.0e+5));
                tv4.setText(String.valueOf(fDato));
                tv5.setText(String.valueOf(fDato*1.0197e+6));
                tv6.setText(String.valueOf(fDato*7.5006e+4));
                break;
            case 5:
                tv1.setText(String.valueOf(fDato*9.806E-5));
                tv2.setText(String.valueOf(fDato*9.678E-5));
                tv3.setText(String.valueOf(fDato*0.09806));
                tv4.setText(String.valueOf(fDato*9.80638E-7));
                tv5.setText(String.valueOf(fDato));
                tv6.setText(String.valueOf(fDato*0.0735539));
                break;
            case 6:
                tv1.setText(String.valueOf(fDato*0.001333224));
                tv2.setText(String.valueOf(fDato*0.001315789));
                tv3.setText(String.valueOf(fDato*1.33324));
                tv4.setText(String.valueOf(fDato*1.333224E-5));
                tv5.setText(String.valueOf(fDato*13.595475));
                tv6.setText(String.valueOf(fDato));
                break;
        }
    }
}