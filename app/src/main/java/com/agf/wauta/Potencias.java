package com.agf.wauta;

import androidx.appcompat.app.AppCompatActivity;

import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class Potencias extends AppCompatActivity {

    private TextView tv1,tv2,tv3,tv4,tv5,tv6;
    private EditText edt_pot;
    private Spinner spinner;
    private String sDato;
    private float fDato;

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_potencias);

        //fija vista en portrait evitar landscape
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //quitar la barra superior de la vista
        getSupportActionBar().hide();

        tv1 = findViewById(R.id.tv_02_potencia);
        tv2 = findViewById(R.id.tv_04_potencia);
        tv3 = findViewById(R.id.tv_06_potencia);
        tv4 = findViewById(R.id.tv_08_potencia);
        tv5 = findViewById(R.id.tv_10_potencia);
        tv6 = findViewById(R.id.tv_12_potencia);
        edt_pot = findViewById(R.id.edt_potencias);

        //para el spinner
        spinner = findViewById(R.id.spinner_potencia);
        String[] opciones = {"Vatio","Kilovatio","Julio-segundo","Kilocaloría-segundo","Kilovoltio·amperio","Caloría-hora"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,R.layout.support_simple_spinner_dropdown_item,opciones);
        spinner.setAdapter(adapter);
    }
    //---------------------------------------------------------------------------------------------- método para evitar excepccion edit-text vacío
    /**
     * método para evitar excepccion edit-text vacío
     * @param _num
     * @return
     */
    public float checkNumero(String _num){
        float resultado = 0f;
        try {
            if (_num!=null){
                resultado = Float.parseFloat(_num);
            }
        }catch (NumberFormatException e){
            Toast.makeText(this,"Error campo vacío",Toast.LENGTH_SHORT).show();
        }
        return resultado;
    }
    //---------------------------------------------------------------------------------------------- método para determinar selección del spinner
    /**
     * método para determinar selección del spinner
     * @param view
     */
    public void seleccionSpinner(View view){
        //activa sonido con click botón flotante
        MediaPlayer mp = MediaPlayer.create(this, R.raw.buttonsounduno);
        mp.start();

        //convertimos dato de entrada de string -> float
        sDato = edt_pot.getText().toString();
        fDato = checkNumero(sDato);

        String seleccion = spinner.getSelectedItem().toString();

        if (seleccion.equals("Vatio")){
            if (fDato!=0f){
                calculaPotencia(1,fDato);
            }else {
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }else if (seleccion.equals("Kilovatio")){
            if (fDato!=0f){
                calculaPotencia(2,fDato);
            }else{
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }else if (seleccion.equals("Julio-segundo")){
            if (fDato!=0f){
                calculaPotencia(3,fDato);
            }else{
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }else if (seleccion.equals("Kilocaloría-segundo")){
            if (fDato!=0f){
                calculaPotencia(4,fDato);
            }else{
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }else if (seleccion.equals("Kilovoltio·amperio")){
            if (fDato!=0f){
                calculaPotencia(5,fDato);
            }else{
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }else if (seleccion.equals("Caloría-hora")){
            if (fDato!=0f){
                calculaPotencia(6,fDato);
            }else{
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }
    }
    //---------------------------------------------------------------------------------------------- método de cálculo de potencias
    /**
     * método de cálculo de potencias
     * W, kW, J/h, kJ/s, kVA, cal/h
     * @param i
     * @param fDato
     */
    private void calculaPotencia(int i, float fDato) {
        switch (i){
            case 1: //W
                tv1.setText(String.valueOf(fDato));
                tv2.setText(String.valueOf(fDato*0.001));
                tv3.setText(String.valueOf(fDato));
                tv4.setText(String.valueOf(fDato*0.000238845));
                tv5.setText(String.valueOf(fDato*0.001));
                tv6.setText(String.valueOf(fDato*859.845227));
                break;
            case 2: //kW
                tv1.setText(String.valueOf(fDato*1000));
                tv2.setText(String.valueOf(fDato));
                tv3.setText(String.valueOf(fDato*1000));
                tv4.setText(String.valueOf(fDato*0.238845896));
                tv5.setText(String.valueOf(fDato));
                tv6.setText(String.valueOf(fDato*8.598e+5));
                break;
            case 3: //J/s
                tv1.setText(String.valueOf(fDato));
                tv2.setText(String.valueOf(fDato*0.001));
                tv3.setText(String.valueOf(fDato));
                tv4.setText(String.valueOf(fDato*0.00023884589));
                tv5.setText(String.valueOf(fDato*0.001));
                tv6.setText(String.valueOf(fDato*859.845227));
                break;
            case 4: //kcal/s
                tv1.setText(String.valueOf(fDato*4186.8));
                tv2.setText(String.valueOf(fDato*4.1868));
                tv3.setText(String.valueOf(fDato*4186.8));
                tv4.setText(String.valueOf(fDato));
                tv5.setText(String.valueOf(fDato*4.1868));
                tv6.setText(String.valueOf(fDato*3.6e+6));
                break;
            case 5: //kVA
                tv1.setText(String.valueOf(fDato*1000));
                tv2.setText(String.valueOf(fDato));
                tv3.setText(String.valueOf(fDato*1000));
                tv4.setText(String.valueOf(fDato*0.238845896));
                tv5.setText(String.valueOf(fDato));
                tv6.setText(String.valueOf(fDato*8.598e+5));
                break;
            case 6: //cal/h
                tv1.setText(String.valueOf(fDato*0.001163));
                tv2.setText(String.valueOf(fDato*1.163e-6));
                tv3.setText(String.valueOf(fDato*0.001163));
                tv4.setText(String.valueOf(fDato*2.77e-7));
                tv5.setText(String.valueOf(fDato*1.163e-6));
                tv6.setText(String.valueOf(fDato));
                break;
        }
    }

}