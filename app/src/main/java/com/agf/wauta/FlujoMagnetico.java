package com.agf.wauta;

import androidx.appcompat.app.AppCompatActivity;

import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class FlujoMagnetico extends AppCompatActivity {

    private TextView tv1,tv2,tv3,tv4;
    private EditText edt_flujomagnetico;
    private Spinner spinner;
    private String sDato;
    private float fDato;

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flujo_magnetico);

        //fija vista en portrait evitar landscape
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //quitar la barra superior de la vista
        getSupportActionBar().hide();

        tv1 = findViewById(R.id.tv_02_flujomagnetico);
        tv2 = findViewById(R.id.tv_04_flujomagnetico);
        tv3 = findViewById(R.id.tv_06_flujomagnetico);
        tv4 = findViewById(R.id.tv_08_flujomagnetico);
        edt_flujomagnetico = findViewById(R.id.edt_flujomagnetico);

        //para las opciones del spinner
        spinner = findViewById(R.id.spinner_flujomagnetico);
        String[] opciones ={"Weber (Wb)","Voltio-segundo (Vs)","Maxell (Mx)","T·m\u00B2"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item,opciones);
        spinner.setAdapter(adapter);
    }
    //---------------------------------------------------------------------------------------------- método para evitar excepccion edit-text vacío
    /**
     * método para evitar excepccion edit-text vacío
     * @param _num
     * @return
     */
    public float checkNumero(String _num){
        float resultado = 0f;
        try {
            if (_num!=null){
                resultado = Float.parseFloat(_num);
            }
        }catch (NumberFormatException e){
            Toast.makeText(this,"Error campo vacío",Toast.LENGTH_SHORT).show();
            MediaPlayer mp1 = MediaPlayer.create(this, R.raw.miedodos);
            mp1.start();
        }
        return resultado;
    }
    //---------------------------------------------------------------------------------------------- método para determinar selección del spinner
    /**
     *  método para determinar selección del spinner
     * @param view
     */
    public void seleccionarSpinner(View view){
        //activa sonido con click botón flotante
        MediaPlayer mp = MediaPlayer.create(this, R.raw.buttonsounduno);
        mp.start();

        //para sonido erróneo de usar 0
        MediaPlayer mp2 = MediaPlayer.create(this, R.raw.miedouno);

        //convertimos dato de entrada de string -> float
        sDato = edt_flujomagnetico.getText().toString();
        fDato = checkNumero(sDato);

        String seleccion = spinner.getSelectedItem().toString();

        if (seleccion.equals("Weber (Wb)")){
            if (fDato!=0f){
                calculoFlujoMagnetico(1,fDato);
            }else{
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
                mp2.start();
            }
        }else if (seleccion.equals("Voltio-segundo (Vs)")){
            if (fDato!=0f){
                calculoFlujoMagnetico(2,fDato);
            }else{
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
                mp2.start();
            }
        }else if (seleccion.equals("Maxell (Mx)")){
            if (fDato!=0f){
                calculoFlujoMagnetico(3,fDato);
            }else{
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
                mp2.start();
            }
        }else if (seleccion.equals("T·m2")){
            if (fDato!=0f){
                calculoFlujoMagnetico(4,fDato);
            }else{
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
                mp2.start();
            }
        }
    }
    //---------------------------------------------------------------------------------------------- método para el cálculo del flujo magnético
    /**
     *  método para el cálculo del flujo magnético
     * @param i
     * @param fDato
     */
    private void calculoFlujoMagnetico(int i, float fDato) {
        switch (i){
            case 1://wb
            case 2://Vs
            case 4://T·m2
                tv1.setText(String.valueOf(fDato));
                tv2.setText(String.valueOf(fDato));
                tv3.setText(String.valueOf(fDato*1.0E+8));
                tv4.setText(String.valueOf(fDato));
                break;
            case 3://Mx
                tv1.setText(String.valueOf(fDato*1.0E-8));
                tv2.setText(String.valueOf(fDato*1.0E-8));
                tv3.setText(String.valueOf(fDato));
                tv4.setText(String.valueOf(fDato*1.0E-8));
                break;
        }
    }
}