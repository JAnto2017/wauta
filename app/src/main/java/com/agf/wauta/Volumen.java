package com.agf.wauta;

import androidx.appcompat.app.AppCompatActivity;

import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class Volumen extends AppCompatActivity {

    private TextView tv1,tv2,tv3,tv4,tv5;
    private EditText edt_volumen;
    private Spinner spinner;
    private String sDato;
    private float fDato;

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_volumen);

        //fija vista en portrait evitar landscape
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //quitar la barra superior de la vista
        getSupportActionBar().hide();

        tv1 = findViewById(R.id.tv_02_volumen);
        tv2 = findViewById(R.id.tv_04_volumen);
        tv3 = findViewById(R.id.tv_06_volumen);
        tv4 = findViewById(R.id.tv_08_volumen);
        tv5 = findViewById(R.id.tv_10_volumen);
        edt_volumen = findViewById(R.id.edt_volumen);

        //las opciones del spinner
        spinner = findViewById(R.id.spinner_volumen);
        String[] opciones = {"Decímetro cúbico (dm3)","Litro (L)","Mililitro (ml)","Metro cúbico (m3)","Milímetro cúbico (mm3)"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item,opciones);
        spinner.setAdapter(adapter);
    }
    //---------------------------------------------------------------------------------------------- método para evitar excepccion edit-text vacío

    /**
     * método para evitar excepccion edit-text vacío
     * @param _num
     * @return
     */
    public float checkNumero(String _num){
        float resultado = 0f;
        try {
            if (_num!=null){
                resultado = Float.parseFloat(_num);
            }
        }catch (NumberFormatException e){
            Toast.makeText(this,"Error campo vacío",Toast.LENGTH_SHORT).show();
        }
        return resultado;
    }
    //---------------------------------------------------------------------------------------------- método para determinar selección del spinner

    /**
     * método para determinar selección del spinner
     * @param view
     */
    public void seleccionarSpinner(View view){
        //activa sonido con click botón flotante
        MediaPlayer mp = MediaPlayer.create(this, R.raw.buttonsounduno);
        mp.start();

        //convertimos dato de entrada de string -> float
        sDato = edt_volumen.getText().toString();
        fDato = checkNumero(sDato);

        String seleccion = spinner.getSelectedItem().toString();

        if (seleccion.equals("Decímetro cúbico (dm3)")){
            if (fDato!=0f){
                calculaVolumen(1,fDato);
            }else{
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }else if (seleccion.equals("Litro (L)")){
            if (fDato!=0f){
                calculaVolumen(2,fDato);
            }else{
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }else if (seleccion.equals("Mililitro (ml)")){
            if (fDato!=0f){
                calculaVolumen(3,fDato);
            }else{
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }else if (seleccion.equals("Metro cúbico (m3)")){
            if (fDato!=0f){
                calculaVolumen(4,fDato);
            }else{
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }else if (seleccion.equals("Milímetro cúbico (mm3)")){
            if (fDato!=0f){
                calculaVolumen(5,fDato);
            }else{
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }
    }
    //---------------------------------------------------------------------------------------------- método para calcular los volúmenes

    /**
     * método para calcular los volúmenes
     * @param i
     * @param fDato
     */
    private void calculaVolumen(int i, float fDato) {
        switch (i){
            case 1:
            case 2:
                tv1.setText(String.valueOf(fDato));
                tv2.setText(String.valueOf(fDato));
                tv3.setText(String.valueOf(fDato*1000));
                tv4.setText(String.valueOf(fDato*0.001));
                tv5.setText(String.valueOf(fDato*1.0e+6));
                break;
            case 3:
                tv1.setText(String.valueOf(fDato*0.001));
                tv2.setText(String.valueOf(fDato*0.001));
                tv3.setText(String.valueOf(fDato));
                tv4.setText(String.valueOf(fDato*1.0E-6));
                tv5.setText(String.valueOf(fDato*1000));
                break;
            case 4:
                tv1.setText(String.valueOf(fDato*1000));
                tv2.setText(String.valueOf(fDato*1000));
                tv3.setText(String.valueOf(fDato*1.0E+6));
                tv4.setText(String.valueOf(fDato));
                tv5.setText(String.valueOf(fDato*1.0E+9));
                break;
            case 5:
                tv1.setText(String.valueOf(fDato*1.0E-6));
                tv2.setText(String.valueOf(fDato*1.0E-6));
                tv3.setText(String.valueOf(fDato*0.001));
                tv4.setText(String.valueOf(fDato*1.0E-9));
                tv5.setText(String.valueOf(fDato));
                break;
        }
    }

}