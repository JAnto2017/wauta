package com.agf.wauta;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Paint;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetBehavior;

public class ResistenciaConductor extends AppCompatActivity {

    BottomSheetBehavior behavior;
    View vistaEmergente;
    private EditText edt_resistividad, edt_longitud, edt_seccion;
    private TextView tv_salida, tv_titulo;
    private String sResistividad, sLongitud, sSeccion;
    private Float fResistividad, fLongitud, fSeccion, fResistenciaCable;

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resistencia_conductor);

        //barra con flecha y quitar texto
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);

        //para el layout emergente
        vistaEmergente = findViewById(R.id.bottomSheet);        //Vista emergente
        behavior = BottomSheetBehavior.from(vistaEmergente);
        behavior.setState(BottomSheetBehavior.STATE_HIDDEN);    //inicial está oculta

        //titulo subrayado
        tv_titulo = findViewById(R.id.tv_resistenciaCable_tit);
        tv_titulo.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);

        edt_resistividad = findViewById(R.id.edt_1_resistenciaCable);
        edt_longitud = findViewById(R.id.edt_2_resistenciaCable);
        edt_seccion = findViewById(R.id.edt_3_resistenciaCable);
        tv_salida = findViewById(R.id.tv_4_resistenciaCable);
    }
    //---------------------------------------------------------------------------------------------- Para los tres botones barra navegación superior
    /**
     * Para los tres botones barra navegación superior
     * @param manu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu manu){
        getMenuInflater().inflate(R.menu.menuopcionescalculos, manu);
        return true;
    }
    //---------------------------------------------------------------------------------------------- Para los tres botones barra navegación superior
    /**
     * Para los tres botones barra navegación superior
     * @param menuItem
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem){

        if (menuItem.getItemId() != R.id.item1 && menuItem.getItemId() != R.id.item2 && menuItem.getItemId() != R.id.item3)
            onBackPressed();    //este es para la flecha izquierda del toolbar

        switch (menuItem.getItemId()){
            case R.id.item1:    //para calcular
                MediaPlayer mp1 = MediaPlayer.create(this,R.raw.buttonsounduno);
                mp1.start();
                calcularResistenciaConductor();
                return true;
            case R.id.item2:    //para layout emergente
                MediaPlayer mp2 = MediaPlayer.create(this,R.raw.claroringtonestono);
                mp2.start();
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                return true;
            case R.id.item3:    //para limpiar
                Toast.makeText(this,"Limpiando selección",Toast.LENGTH_SHORT).show();
                MediaPlayer mp3 = MediaPlayer.create(this,R.raw.sdalert20);
                mp3.start();
                limpiar();
                return true;
        }
        return true;
    }
    //---------------------------------------------------------------------------------------------- método para limpiar los EditText del layout
    /**
     * método para limpiar los EditText del layout
     */
    private void limpiar() {
        edt_resistividad.setText("");
        edt_seccion.setText("");
        edt_longitud.setText("");
        tv_salida.setText("_");
    }
    //---------------------------------------------------------------------------------------------- método para evitar excepccion edit-text vacío
    /**
     * método para evitar excepccion edit-text vacío
     * @param _num
     * @return
     */
    public float checkNumero(String _num){
        float resultado = 0.0f;
        try {
            if (_num!=null){
                resultado = Float.parseFloat(_num);
            }
        }catch (NumberFormatException e){
            Toast mytoasErrot = Toast.makeText(this,"Error campo de entrada de datos vacío",Toast.LENGTH_LONG);
            mytoasErrot.setGravity(Gravity.CENTER,0,0);
            mytoasErrot.show();
            MediaPlayer mp4 = MediaPlayer.create(this,R.raw.miedodos);
            mp4.start();
        }
        return resultado;
    }
    //---------------------------------------------------------------------------------------------- método para realizar el cálculo de la resistencia del cable
    /**
     * método para realizar el cálculo de la resistencia del cable
     */
    private void calcularResistenciaConductor() {
        sResistividad = edt_resistividad.getText().toString();
        fResistividad = checkNumero(sResistividad);
        sLongitud = edt_longitud.getText().toString();
        fLongitud = checkNumero(sLongitud);
        sSeccion = edt_seccion.getText().toString();
        fSeccion = checkNumero(sSeccion);

        if (fSeccion!=0f){
            fResistenciaCable = fResistividad * fLongitud / fSeccion;
        }else{
            Toast miToastDiv0 = Toast.makeText(ResistenciaConductor.this,"La Sección NO puede ser 0",Toast.LENGTH_SHORT);
            miToastDiv0.setGravity(Gravity.CENTER,0,0);
            miToastDiv0.show();
        }

        activaAnimacion(fResistenciaCable);
    }
    //---------------------------------------------------------------------------------------------- método para activar animación de salida en TextView
    /**
     * método para activar animación de salida en TextView
     * @param _dato
     */
    public void activaAnimacion(Float _dato){
        tv_salida.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade_suave));
        tv_salida.setText(String.valueOf(_dato) + " Ohm");
    }
    //---------------------------------------------------------------------------------------------- botón dentro de layout emergente~
    /**
     * botón dentro de layout emergente~
     * @param view
     */
    public void cerrarSheet(View view){
        behavior.setState(BottomSheetBehavior.STATE_HIDDEN);
    }
}