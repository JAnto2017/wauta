package com.agf.wauta;

import androidx.appcompat.app.AppCompatActivity;

import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class FlujoLuminoso extends AppCompatActivity {

    private TextView tv1,tv2,tv3,tv4,tv5;
    private EditText edt_flujolum;
    private Spinner spinner;
    private String sDato;
    private float fDato;

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flujo_luminoso);

        //fija vista en portrait evitar landscape
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //quitar la barra superior de la vista
        getSupportActionBar().hide();

        tv1 = findViewById(R.id.tv_02_flujolum);
        tv2 = findViewById(R.id.tv_04_flujolum);
        tv3 = findViewById(R.id.tv_06_flujolum);
        tv4 = findViewById(R.id.tv_08_flujolum);
        tv5 = findViewById(R.id.tv_10_flujolum);
        edt_flujolum = findViewById(R.id.edt_flujoluminoso);

        //para las opciones del spinner
        spinner = findViewById(R.id.spinner_flujolum);
        String[] opciones ={"Lumen (lm)","Lux·m\u00B2","cd·sr","Vatio Lámpara LED","Vatio Lámpara Halógena"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item,opciones);
        spinner.setAdapter(adapter);
    }
    //---------------------------------------------------------------------------------------------- método para evitar excepccion edit-text vacío
    /**
     * método para evitar excepccion edit-text vacío
     * @param _num
     * @return
     */
    public float checkNumero(String _num){
        float resultado = 0f;
        try {
            if (_num!=null){
                resultado = Float.parseFloat(_num);
            }
        }catch (NumberFormatException e){
            Toast.makeText(this,"Error campo vacío",Toast.LENGTH_SHORT).show();
            MediaPlayer mp1 = MediaPlayer.create(this, R.raw.miedodos);
            mp1.start();
        }
        return resultado;
    }
    //---------------------------------------------------------------------------------------------- método para determinar selección del spinner
    /**
     * método para determinar selección del spinner
     * @param view
     */
    public void seleccionarSpinner(View view){
        //activa sonido con click botón flotante
        MediaPlayer mp = MediaPlayer.create(this, R.raw.buttonsounduno);
        mp.start();

        //para sonido erróneo de usar 0
        MediaPlayer mp2 = MediaPlayer.create(this, R.raw.miedouno);

        //convertimos dato de entrada de string -> float
        sDato = edt_flujolum.getText().toString();
        fDato = checkNumero(sDato);

        String seleccion = spinner.getSelectedItem().toString();

        if (seleccion.equals("Lumen (lm)")){
            if (fDato!=0f){
                calculoFlujoLuminoso(1,fDato);
            }else{
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
                mp2.start();
            }
        }else if (seleccion.equals("Lux·m2")){
            if (fDato!=0f){
                calculoFlujoLuminoso(2,fDato);
            }else{
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
                mp2.start();
            }
        }else if(seleccion.equals("cd·sr")){
            if (fDato!=0f){
                calculoFlujoLuminoso(3,fDato);
            }else{
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
                mp2.start();
            }
        }else if (seleccion.equals("Vatio Lámpara LED")){
            if (fDato!=0f){
                calculoFlujoLuminoso(4,fDato);
            }else{
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
                mp2.start();
            }
        }else if (seleccion.equals("Vatio Lámpara Halógena")){
            if (fDato!=0f){
                calculoFlujoLuminoso(5,fDato);
            }else{
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
                mp2.start();
            }
        }
    }
    //----------------------------------------------------------------------------------------------
    /**
     * método de cálculo del flujo luminoso
     * @param i
     * @param fDato
     */
    private void calculoFlujoLuminoso(int i, float fDato) {
        switch (i){
            case 1:
            case 2:
            case 3:
                tv1.setText(String.valueOf(fDato));
                tv2.setText(String.valueOf(fDato));
                tv3.setText(String.valueOf(fDato));
                tv4.setText(String.valueOf(fDato*0.00556));
                tv5.setText(String.valueOf(fDato*0.05));
                break;
            case 4:
                tv1.setText(String.valueOf(fDato*180));
                tv2.setText(String.valueOf(fDato*180));
                tv3.setText(String.valueOf(fDato*180));
                tv4.setText(String.valueOf(fDato));
                tv5.setText(String.valueOf(fDato*9));
                break;
            case 5:
                tv1.setText(String.valueOf(fDato*20));
                tv2.setText(String.valueOf(fDato*20));
                tv3.setText(String.valueOf(fDato*20));
                tv4.setText(String.valueOf(fDato*0.111));
                tv5.setText(String.valueOf(fDato));
                break;
        }
    }
}