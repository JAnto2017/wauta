package com.agf.wauta;

import androidx.appcompat.app.AppCompatActivity;

import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class Prefijos extends AppCompatActivity {

    private TextView tv1,tv2,tv3,tv4,tv5,tv6,tv7,tv8,tv9;
    private EditText edt_prefijos;
    private Spinner spinner;
    private String sDato;
    private float fDato;

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prefijos);

        //fija vista en portrait evitar landscape
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //quitar la barra superior de la vista
        getSupportActionBar().hide();

        tv1 = findViewById(R.id.tv_02_prefijos);
        tv2 = findViewById(R.id.tv_04_prefijos);
        tv3 = findViewById(R.id.tv_06_prefijos);
        tv4 = findViewById(R.id.tv_08_prefijos);
        tv5 = findViewById(R.id.tv_10_prefijos);
        tv6 = findViewById(R.id.tv_12_prefijos);
        tv7 = findViewById(R.id.tv_14_prefijos);
        tv8 = findViewById(R.id.tv_16_prefijos);
        tv9 = findViewById(R.id.tv_18_prefijos);
        edt_prefijos = findViewById(R.id.edt_prefijos);

        //para las opciones del spinner
        spinner = findViewById(R.id.spinner_prefijos);
        String[] opciones ={"Peta (P)","Tera (T)","Giga (G)","Mega (M)","Kilo (k)","mili (m)","micro (u)","nano (n)","pico (p)"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item,opciones);
        spinner.setAdapter(adapter);
    }
    //---------------------------------------------------------------------------------------------- método para evitar excepccion edit-text vacío
    /**
     * método para evitar excepccion edit-text vacío
     * @param _num
     * @return
     */
    public float checkNumero(String _num){
        float resultado = 0f;
        try {
            if (_num!=null){
                resultado = Float.parseFloat(_num);
            }
        }catch (NumberFormatException e){
            Toast.makeText(this,"Error campo vacío",Toast.LENGTH_SHORT).show();
        }
        return resultado;
    }
    //---------------------------------------------------------------------------------------------- método para determinar selección del spinner
    /**
     * método para determinar selección del spinner
     * @param view
     */
    public void seleccionarSpinner(View view){
        //activa sonido con click botón flotante
        MediaPlayer mp = MediaPlayer.create(this, R.raw.buttonsounduno);
        mp.start();

        //convertimos dato de entrada de string -> float
        sDato = edt_prefijos.getText().toString();
        fDato = checkNumero(sDato);

        String seleccion = spinner.getSelectedItem().toString();

        if (seleccion.equals("Peta (P)")){
            if (fDato!=0f){
                calculaPrefijos(1,fDato);
            }else{
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }else if (seleccion.equals("Tera (T)")){
            if (fDato!=0f){
                calculaPrefijos(2,fDato);
            }else{
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }else if (seleccion.equals("Giga (G)")){
            if (fDato!=0f){
                calculaPrefijos(3,fDato);
            }else{
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }else if (seleccion.equals("Mega (M)")){
            if (fDato!=0f){
                calculaPrefijos(4,fDato);
            }else{
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }else if (seleccion.equals("Kilo (k)")){
            if (fDato!=0f){
                calculaPrefijos(5,fDato);
            }else{
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }else if (seleccion.equals("mili (m)")){
            if (fDato!=0f){
                calculaPrefijos(6,fDato);
            }else{
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }else if (seleccion.equals("micro (u)")){
            if (fDato!=0f){
                calculaPrefijos(7,fDato);
            }else{
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }else if (seleccion.equals("nano (n)")){
            if (fDato!=0f){
                calculaPrefijos(8,fDato);
            }else{
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }else if (seleccion.equals("pico (p)")){
            if (fDato!=0f){
                calculaPrefijos(9,fDato);
            }else{
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }
    }
    //---------------------------------------------------------------------------------------------- método de cálculo para los prefijos
    /**
     * método de cálculo para los prefijos
     * @param i
     * @param fDato
     */
    private void calculaPrefijos(int i, float fDato) {
        switch (i){
            case 1: //Peta
                tv1.setText(String.valueOf(fDato));
                tv2.setText(String.valueOf(fDato*1E+3));
                tv3.setText(String.valueOf(fDato*1E+6));
                tv4.setText(String.valueOf(fDato*1E+9));
                tv5.setText(String.valueOf(fDato*1E+12));
                tv6.setText(String.valueOf(fDato*1E+15));
                tv7.setText(String.valueOf(fDato*1E+18));
                tv8.setText(String.valueOf(fDato*1E+21));
                tv9.setText(String.valueOf(fDato*1E+24));
                break;
            case 2: //Tera
                tv1.setText(String.valueOf(fDato/1E+6));
                tv2.setText(String.valueOf(fDato));
                tv3.setText(String.valueOf(fDato*1E+3));
                tv4.setText(String.valueOf(fDato*1E+6));
                tv5.setText(String.valueOf(fDato*1E+9));
                tv6.setText(String.valueOf(fDato*1E+12));
                tv7.setText(String.valueOf(fDato*1E+15));
                tv8.setText(String.valueOf(fDato*1E+18));
                tv9.setText(String.valueOf(fDato*1E+21));
                break;
            case 3: //Giga
                tv1.setText(String.valueOf(fDato/1E+6));
                tv2.setText(String.valueOf(fDato/1E+3));
                tv3.setText(String.valueOf(fDato));
                tv4.setText(String.valueOf(fDato*1E+3));
                tv5.setText(String.valueOf(fDato*1E+6));
                tv6.setText(String.valueOf(fDato*1E+9));
                tv7.setText(String.valueOf(fDato*1E+12));
                tv8.setText(String.valueOf(fDato*1E+15));
                tv9.setText(String.valueOf(fDato+1E+18));
                break;
            case 4: //Mega
                tv1.setText(String.valueOf(fDato/1E+9));
                tv2.setText(String.valueOf(fDato/1E+6));
                tv3.setText(String.valueOf(fDato/1E+3));
                tv4.setText(String.valueOf(fDato));
                tv5.setText(String.valueOf(fDato*1E+3));
                tv6.setText(String.valueOf(fDato*1E+6));
                tv7.setText(String.valueOf(fDato*1E+9));
                tv8.setText(String.valueOf(fDato*1E+12));
                tv9.setText(String.valueOf(fDato*1E+15));
                break;
            case 5: //kilo
                tv1.setText(String.valueOf(fDato/1E+12));
                tv2.setText(String.valueOf(fDato/1E+9));
                tv3.setText(String.valueOf(fDato/1E+6));
                tv4.setText(String.valueOf(fDato/1E+3));
                tv5.setText(String.valueOf(fDato));
                tv6.setText(String.valueOf(fDato*1E+3));
                tv7.setText(String.valueOf(fDato*1E+6));
                tv8.setText(String.valueOf(fDato*1E+9));
                tv9.setText(String.valueOf(fDato*1E+12));
                break;
            case 6: //mili
                tv1.setText(String.valueOf(fDato/1E+15));
                tv2.setText(String.valueOf(fDato/1E+12));
                tv3.setText(String.valueOf(fDato/1E+9));
                tv4.setText(String.valueOf(fDato/1E+6));
                tv5.setText(String.valueOf(fDato/1E+3));
                tv6.setText(String.valueOf(fDato));
                tv7.setText(String.valueOf(fDato*1E+3));
                tv8.setText(String.valueOf(fDato*1E+6));
                tv9.setText(String.valueOf(fDato*1E+9));
                break;
            case 7: //micro
                tv1.setText(String.valueOf(fDato/1E+18));
                tv2.setText(String.valueOf(fDato/1E+15));
                tv3.setText(String.valueOf(fDato/1E+12));
                tv4.setText(String.valueOf(fDato/1E+9));
                tv5.setText(String.valueOf(fDato/1E+6));
                tv6.setText(String.valueOf(fDato/1E+3));
                tv7.setText(String.valueOf(fDato));
                tv8.setText(String.valueOf(fDato*1E+3));
                tv9.setText(String.valueOf(fDato*1E+6));
                break;
            case 8: //nano
                tv1.setText(String.valueOf(fDato/1E+21));
                tv2.setText(String.valueOf(fDato/1E+18));
                tv3.setText(String.valueOf(fDato/1E+15));
                tv4.setText(String.valueOf(fDato/1E+12));
                tv5.setText(String.valueOf(fDato/1E+9));
                tv6.setText(String.valueOf(fDato/1E+6));
                tv7.setText(String.valueOf(fDato/1E+3));
                tv8.setText(String.valueOf(fDato));
                tv9.setText(String.valueOf(fDato*1E+3));
                break;
            case 9: //pico
                tv1.setText(String.valueOf(fDato/1E+24));
                tv2.setText(String.valueOf(fDato/1E+21));
                tv3.setText(String.valueOf(fDato/1E+18));
                tv4.setText(String.valueOf(fDato/1E+15));
                tv5.setText(String.valueOf(fDato/1E+12));
                tv6.setText(String.valueOf(fDato/1E+9));
                tv7.setText(String.valueOf(fDato/1E+6));
                tv8.setText(String.valueOf(fDato/1E+3));
                tv9.setText(String.valueOf(fDato));
                break;
        }
    }
}