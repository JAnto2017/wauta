package com.agf.wauta;

import androidx.appcompat.app.AppCompatActivity;

import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class Longitud extends AppCompatActivity {

    private Spinner spinner;
    private String sDato;
    private float fDato;
    private TextView tv1,tv2,tv3,tv4,tv5,tv6,tv7,tv8,tv9,tv10;
    private EditText edt_longitud;

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_longitud);

        //fija vista en portrait evitar landscape
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //quitar la barra superior de la vista
        getSupportActionBar().hide();

        tv1 = findViewById(R.id.tv_02_long);
        tv2 = findViewById(R.id.tv_04_long);
        tv3 = findViewById(R.id.tv_06_long);
        tv4 = findViewById(R.id.tv_08_long);
        tv5 = findViewById(R.id.tv_10_long);
        tv6 = findViewById(R.id.tv_12_long);
        tv7 = findViewById(R.id.tv_14_long);
        tv8 = findViewById(R.id.tv_16_long);
        tv9 = findViewById(R.id.tv_18_long);
        tv10= findViewById(R.id.tv_20_long);
        edt_longitud = findViewById(R.id.edt_longitud);

        //para el spinner
        spinner = findViewById(R.id.spinner_long);
        String[] opciones = {"Kilómetros","Hectómetro","Decámetro","Metro","Decímetro","Centímetro","Milímetro","Yarda","Pies","Pulgadas"};
        ArrayAdapter<String> _adapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, opciones);
        spinner.setAdapter(_adapter);

    }
    //---------------------------------------------------------------------------------------------- método para evitar excepccion edit-text vacío
    /**
     * método para evitar excepccion edit-text vacío
     * @param _num
     * @return
     */
    public float checkNumero(String _num){
        float resultado = 0f;
        try {
            if (_num!=null){
                resultado = Float.parseFloat(_num);
            }
        }catch (NumberFormatException e){
            Toast.makeText(this,"Error campo vacío",Toast.LENGTH_SHORT).show();
        }
        return resultado;
    }
    //---------------------------------------------------------------------------------------------- método para el botón flotante
    /**
     * método para el botón flotante
     * @param view
     */
    public void realizaCalculos(View view){
        //activa sonido con click botón flotante
        MediaPlayer mp = MediaPlayer.create(this, R.raw.buttonsounduno);
        mp.start();

        //convertimos dato de entrada de string -> float
        sDato = edt_longitud.getText().toString();
        fDato = checkNumero(sDato);

        String seleccion = spinner.getSelectedItem().toString();

        if (seleccion.equals("Kilómetro")){
            if (fDato!=0f){
                calcLongitud(1, fDato);
            }else {
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }else if (seleccion.equals("Hectómetro")){
            if (fDato!=0f){
                calcLongitud(2, fDato);
            }else {
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }else if (seleccion.equals("Decámetro")){
            if (fDato!=0f){
                calcLongitud(3, fDato);
            }else {
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }else if (seleccion.equals("Metro")){
            if (fDato!=0f){
                calcLongitud(4, fDato);
            }else {
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }else if (seleccion.equals("Decímetro")){
            if (fDato!=0f){
                calcLongitud(5, fDato);
            }else {
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }else if (seleccion.equals("Centímetro")){
            if (fDato!=0f){
                calcLongitud(6, fDato);
            }else {
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }else if (seleccion.equals("Milímetro")){
            if (fDato!=0f){
                calcLongitud(7, fDato);
            }else {
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }else if (seleccion.equals("Yarda")){
            if (fDato!=0f){
                calcLongitud(8, fDato);
            }else {
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }else if (seleccion.equals("Pies")){
            if (fDato!=0f){
                calcLongitud(9, fDato);
            }else {
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }else if (seleccion.equals("Pulgada")){
            if (fDato!=0f){
                calcLongitud(10, fDato);
            }else {
                Toast.makeText(this,"El dato de entrada no puede ser 0",Toast.LENGTH_SHORT).show();
            }
        }
    }
    //---------------------------------------------------------------------------------------------- método de los cálculos de longitudes
    /**
     * método de los cálculos de longitudes
     * Km, Hm, Dam, m, dm, cm, mm, yd, ft, in
     * @param _i
     * @param _fDato
     */
    protected void calcLongitud(int _i, float _fDato) {
        switch (_i){
            case 1: //km
                tv1.setText(String.valueOf(_fDato));
                tv2.setText(String.valueOf(_fDato*10));
                tv3.setText(String.valueOf(_fDato*100));
                tv4.setText(String.valueOf(_fDato*1000));
                tv5.setText(String.valueOf(_fDato*10000));
                tv6.setText(String.valueOf(_fDato*1.0e+5));
                tv7.setText(String.valueOf(_fDato*1.0e+6));
                tv8.setText(String.valueOf(_fDato*1093.613298));
                tv9.setText(String.valueOf(_fDato*3280.839895));
                tv10.setText(String.valueOf(_fDato*39370.07874));
                break;
            case 2: //hm
                tv1.setText(String.valueOf(_fDato*0.1));
                tv2.setText(String.valueOf(_fDato));
                tv3.setText(String.valueOf(_fDato*10));
                tv4.setText(String.valueOf(_fDato*100));
                tv5.setText(String.valueOf(_fDato*1000));
                tv6.setText(String.valueOf(_fDato*10000));
                tv7.setText(String.valueOf(_fDato*1.0e+5));
                tv8.setText(String.valueOf(_fDato*109.3613298));
                tv9.setText(String.valueOf(_fDato*328.0839895));
                tv10.setText(String.valueOf(_fDato*3937.007874));
                break;
            case 3: //dam
                tv1.setText(String.valueOf(_fDato*0.01));
                tv2.setText(String.valueOf(_fDato*0.1));
                tv3.setText(String.valueOf(_fDato));
                tv4.setText(String.valueOf(_fDato*10));
                tv5.setText(String.valueOf(_fDato*100));
                tv6.setText(String.valueOf(_fDato*1000));
                tv7.setText(String.valueOf(_fDato*10000));
                tv8.setText(String.valueOf(_fDato*10.93613298));
                tv9.setText(String.valueOf(_fDato*32.80839895));
                tv10.setText(String.valueOf(_fDato*393.7007874));
                break;
            case 4: //m
                tv1.setText(String.valueOf(_fDato*0.001));
                tv2.setText(String.valueOf(_fDato*0.01));
                tv3.setText(String.valueOf(_fDato*0.1));
                tv4.setText(String.valueOf(_fDato));
                tv5.setText(String.valueOf(_fDato*10));
                tv6.setText(String.valueOf(_fDato*100));
                tv7.setText(String.valueOf(_fDato*1000));
                tv8.setText(String.valueOf(_fDato*1.09361329));
                tv9.setText(String.valueOf(_fDato*3.28083989));
                tv10.setText(String.valueOf(_fDato*39.3700787));
                break;
            case 5: //dm
                tv1.setText(String.valueOf(_fDato*0.0001));
                tv2.setText(String.valueOf(_fDato*0.001));
                tv3.setText(String.valueOf(_fDato*0.01));
                tv4.setText(String.valueOf(_fDato*0.1));
                tv5.setText(String.valueOf(_fDato));
                tv6.setText(String.valueOf(_fDato*10));
                tv7.setText(String.valueOf(_fDato*100));
                tv8.setText(String.valueOf(_fDato*0.109361329));
                tv9.setText(String.valueOf(_fDato*0.328083989));
                tv10.setText(String.valueOf(_fDato*3.937007874));
                break;
            case 6: //cm
                tv1.setText(String.valueOf(_fDato*1.0e-5));
                tv2.setText(String.valueOf(_fDato*0.0001));
                tv3.setText(String.valueOf(_fDato*0.001));
                tv4.setText(String.valueOf(_fDato*0.01));
                tv5.setText(String.valueOf(_fDato*0.1));
                tv6.setText(String.valueOf(_fDato));
                tv7.setText(String.valueOf(_fDato*10));
                tv8.setText(String.valueOf(_fDato*0.010936133));
                tv9.setText(String.valueOf(_fDato*0.0328083989));
                tv10.setText(String.valueOf(_fDato*0.3937007874));
                break;
            case 7: //mm
                tv1.setText(String.valueOf(_fDato*1.0e-6));
                tv2.setText(String.valueOf(_fDato*1.0e-5));
                tv3.setText(String.valueOf(_fDato*0.0001));
                tv4.setText(String.valueOf(_fDato*0.001));
                tv5.setText(String.valueOf(_fDato*0.01));
                tv6.setText(String.valueOf(_fDato*0.1));
                tv7.setText(String.valueOf(_fDato));
                tv8.setText(String.valueOf(_fDato*0.0010936133));
                tv9.setText(String.valueOf(_fDato*0.00328083989));
                tv10.setText(String.valueOf(_fDato*0.0393700787));
                break;
            case 8: //yd
                tv1.setText(String.valueOf(_fDato*0.0009144));
                tv2.setText(String.valueOf(_fDato*0.009144));
                tv3.setText(String.valueOf(_fDato*0.09144));
                tv4.setText(String.valueOf(_fDato*0.9144));
                tv5.setText(String.valueOf(_fDato*9.144));
                tv6.setText(String.valueOf(_fDato*91.44));
                tv7.setText(String.valueOf(_fDato*914.4));
                tv8.setText(String.valueOf(_fDato));
                tv9.setText(String.valueOf(_fDato*3));
                tv10.setText(String.valueOf(_fDato*36));
                break;
            case 9: //ft
                tv1.setText(String.valueOf(_fDato*0.0003048));
                tv2.setText(String.valueOf(_fDato*0.003048));
                tv3.setText(String.valueOf(_fDato*0.03048));
                tv4.setText(String.valueOf(_fDato*0.3048));
                tv5.setText(String.valueOf(_fDato*3.048));
                tv6.setText(String.valueOf(_fDato*30.48));
                tv7.setText(String.valueOf(_fDato*304.8));
                tv8.setText(String.valueOf(_fDato*1/3f));
                tv9.setText(String.valueOf(_fDato));
                tv10.setText(String.valueOf(_fDato*12));
                break;
            case 10: //in
                tv1.setText(String.valueOf(_fDato*2.54e-5));
                tv2.setText(String.valueOf(_fDato*0.000254));
                tv3.setText(String.valueOf(_fDato*0.00254));
                tv4.setText(String.valueOf(_fDato*0.0254));
                tv5.setText(String.valueOf(_fDato*0.254));
                tv6.setText(String.valueOf(_fDato*2.54));
                tv7.setText(String.valueOf(_fDato*25.4));
                tv8.setText(String.valueOf(_fDato*0.02777777));
                tv9.setText(String.valueOf(_fDato*0.08333333));
                tv10.setText(String.valueOf(_fDato));
                break;
        }
    }
}