package com.agf.wauta;

import org.junit.Test;

import static org.junit.Assert.*;

import android.view.Gravity;
import android.widget.Toast;

/**
 * Pruebas unitarias de los métodos usados
 *
 * JUnit 4.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {


    //----------------------------------------------------------------------------------------------------------------------------------------------------
    public double PotenciaActi(int rb1,double fCorriente,double fFactorPot, double fVoltio) {
        if (rb1==1){
            //opción a 230 V
            double esperado = 172.5d;
            double delta = 1.0d;
            double calculado = fCorriente * 230 * fFactorPot;
            return calculado;

        }else if (rb1==2){
            //opción a 400 V
            double esperado = 1.0d;
            double delta = 1.0d;
            double calculado = fCorriente * 400 * fFactorPot;
            return calculado;

        }else if (rb1!=1 && rb1!=2){
            //opción libre
            double esperado = 1.0d;
            double delta = 1.0d;
            double calculado = fVoltio * fCorriente * fFactorPot;
            return calculado;
        }
        return 0;
    }
    @Test(timeout = 10)
    public void test_PotenciaActi(){
        double esperado = PotenciaActi(1,1.5,0.5,230);
        double actual = 172.5d;
        double delta = 0.5;
        assertEquals(esperado,actual,delta);
    }
    //----------------------------------------------------------------------------------------------------------------------------------------------------
    private float calculoDivisorTension(float fR1, float fR2, float fVoltaje) {
        float fDivTension = 0.0f;
        if ((fR1+fR2)!=0f){
            fDivTension = fVoltaje * fR1 / (fR1 + fR2);
        }
        return fDivTension;
    }
    //@Test(expected = ArithmeticException.class)
    @Test
    public void test_calculoDivisorTension(){
        float esper = (float) calculoDivisorTension(0, 355, 25);
        float actua = 0.0f;
        float delta = 0.5f;
        assertEquals(esper,actua,delta);
    }
    //----------------------------------------------------------------------------------------------------------------------------------------------------
    private double calculoAnguloRLCserie(float fXL, float fXC, float fR) {
        if (fR != 0){
            double dAnguloSerie = Math.toDegrees(Math.atan((fXL - fXC) / fR));
            double dAnguloSerieSinDecim = Math.round(dAnguloSerie * 100d) /100d;
            return dAnguloSerieSinDecim;
        }else {
            //Toast.makeText(this,"El valor de R no puede ser 0",Toast.LENGTH_SHORT).show();
            return 0;
        }
    }
    @Test(timeout = 10)
    public void test_CalculoAnguloRLCserie() {
        double expected = calculoAnguloRLCserie(3.3f,1.8f,120.0f);
        double actual = 0.72;
        double delta = 0.1d;
        assertEquals(expected, actual, delta);
    }
    //----------------------------------------------------------------------------------------------------------------------------------------------------
}