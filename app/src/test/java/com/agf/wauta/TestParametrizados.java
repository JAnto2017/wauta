package com.agf.wauta;

import static org.junit.Assert.assertEquals;

import android.app.Presentation;
import android.widget.Toast;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.ArrayList;
import java.util.List;

@RunWith(value = Parameterized.class)
public class TestParametrizados {
    /*
     * opción más sencilla es usando el siguiente Arrays.asList
     * Arrays.asList(new Object[][] { {3,1,4}, {2,3,5}, {3,3,6}});
     */
    //--------------------------------------------------------------------
    /**
     * ArrayList para asignar varios valores de forma secuencial
     * @return
     */
    @Parameterized.Parameters
    public static Iterable<Object[]> getData() {
        List<Object[]> obj = new ArrayList<>();
        obj.add(new Object[] {5,5,11,11.0f});
        obj.add(new Object[] {0,1,25,0.0f});
        obj.add(new Object[] {33,1,37,1221.0f});
        obj.add(new Object[] {3,1,3,9.0f});
        return obj;
    }
    //----------------------------------------------------------------------------------------------
    //atributos globales
    private int fI,fN,fFlujo;
    private float expected;
    //----------------------------------------------------------------------------------------------
    //constructor
    public TestParametrizados(int a,int b,int c,float expected) {
        fN=a;
        fI=b;
        fFlujo=c;
        this.expected=expected;
    }
    //----------------------------------------------------------------------------------------------
    /**
     * Método a comprobar con prueba unitaria
     * @param fI
     * @param fN
     * @param fFlujo
     * @return
     */
    public float _coeficienteAutoinduccion(int fN,int fI, int fFlujo) {
        float fCoefAutoinduc = 0.0f;
        if (fI != 0){
            fCoefAutoinduc = (float) (fN * fFlujo) / fI;
            //return (fCoefAutoinduc);
        } //else{
            //Toast.makeText(this,"La Intensidad de la corriente NO puede ser 0",Toast.LENGTH_SHORT).show();
        //}
        return fCoefAutoinduc;
    }
    //----------------------------------------------------------------------------------------------
    /**
     * Test sobre el método
     */
    @Test
    public void _test() {
        float result = _coeficienteAutoinduccion(fN,fI,fFlujo);
        float delta = 11.5f;
        assertEquals(expected,result,delta);
    }
    //----------------------------------------------------------------------------------------------
}
